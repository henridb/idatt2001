module edu.ntnu.idatt2001 {

  requires javafx.graphics;
  requires javafx.controls;
  requires org.controlsfx.controls;

  exports edu.ntnu.idatt2001;
  exports edu.ntnu.idatt2001.codec;
  exports edu.ntnu.idatt2001.file;
  exports edu.ntnu.idatt2001.model.action;
  exports edu.ntnu.idatt2001.model.game;
  exports edu.ntnu.idatt2001.model.goal;
  exports edu.ntnu.idatt2001.page;
  exports edu.ntnu.idatt2001.util;
  exports edu.ntnu.idatt2001.bootstrap;
}