package edu.ntnu.idatt2001.bootstrap;

import edu.ntnu.idatt2001.Paths;

/**
 * Bootstrap class for running the application.
 */
public final class Bootstrap {

  /**
   * The main class start method.
   *
   * @param   args program args
   */
  public static void main(String[] args) {
    Paths.main(args);
  }
}
