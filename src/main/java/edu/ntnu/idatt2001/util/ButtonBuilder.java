package edu.ntnu.idatt2001.util;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * A builder for JavaFX buttons.
 */
public final class ButtonBuilder {

  private String text;
  private boolean disabled;
  private Node alignmentParent;
  private Node marginParent;
  private Pos alignment;
  private Insets margin;
  private Node child;
  private ButtonStyle style;
  private EventHandler<? super MouseEvent> onClick;

  /**
   * The text to display.
   *
   * @param   text the text
   * @return  the builder
   */
  public ButtonBuilder text(String text) {
    this.text = text;
    return this;
  }

  /**
   * Whether the button should be disabled.
   *
   * @param   disabled true if the button should be disabled
   * @return  the builder
   */
  public ButtonBuilder disabled(boolean disabled) {
    this.disabled = disabled;
    return this;
  }

  /**
   * Set the alignment of the button that lies in the
   * specified parent node.
   *
   * @param   parent the parent node of the button
   * @param   alignment the alignment
   * @return  the builder
   */
  public ButtonBuilder alignment(Node parent, Pos alignment) {
    this.alignmentParent = parent;
    this.alignment = alignment;
    return this;
  }

  /**
   * Set the margin of the buttton that lies in the specified
   * parent node.
   *
   * @param   parent the parent node of the button
   * @param   margin the margin
   * @return  the builder
   */
  public ButtonBuilder margin(Node parent, double margin) {
    this.marginParent = parent;
    this.margin = new Insets(margin);
    return this;
  }

  /**
   * Set the margin of the buttton that lies in the specified
   * parent node.
   *
   * @param   parent the parent node of the button
   * @param   top the top margin
   * @param   right the right margin
   * @param   bottom the bottom margin
   * @param   left the left margin
   * @return  the builder
   */
  public ButtonBuilder margin(Node parent, double top, double right, double bottom, double left) {
    this.marginParent = parent;
    this.margin = new Insets(top, right, bottom, left);
    return this;
  }

  /**
   * Set the child node of the button.
   *
   * @param   child the child node
   * @return  the builder
   */
  public ButtonBuilder child(Node child) {
    this.child = child;
    return this;
  }

  /**
   * Set the style of the button.
   *
   * @param   style the button style
   * @return  the builder
   * @see     ButtonStyle
   */
  public ButtonBuilder style(ButtonStyle style) {
    this.style = style;
    return this;
  }

  /**
   * Set the mouse click event.
   *
   * @param   onClick the event
   * @return  the builder
   */
  public ButtonBuilder onClick(EventHandler<? super MouseEvent> onClick) {
    this.onClick = onClick;
    return this;
  }

  /**
   * Build the button.
   *
   * @return  a JavaFX button
   */
  public Button build() {
    Button button = new Button(text, child);
    button.setOnMouseClicked(onClick);
    button.setDisable(disabled);

    // Set alignment
    if (alignmentParent instanceof BorderPane) {
      BorderPane.setAlignment(button, alignment);
    } else {
      button.setAlignment(alignment);
    }

    // Set margin
    if (marginParent instanceof VBox) {
      VBox.setMargin(button, margin);
    } else if (marginParent instanceof HBox) {
      HBox.setMargin(button, margin);
    } else if (marginParent instanceof BorderPane) {
      BorderPane.setMargin(button, margin);
    }

    if (style != null) {
      FxUtils.applyStyle(button, style.getPath());
    }

    button.setMinWidth(Control.USE_PREF_SIZE);
    return button;
  }
}
