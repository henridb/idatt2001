package edu.ntnu.idatt2001.util;

import java.util.List;

/**
 * A utility class for methods handling strings.
 */
public final class StringUtils {

  /**
   * Returns a string of the elements in the given list
   * with a delimiter between them.
   *
   * @param   delimiter the delimiter
   * @param   objects the objects
   * @return  a string of concatenated elements
   */
  public static String join(String delimiter, List<?> objects) {
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < objects.size(); i++) {
      builder.append(objects.get(i));
      if (i < objects.size() - 1) {
        builder.append(delimiter);
      }
    }
    return builder.toString();
  }
}
