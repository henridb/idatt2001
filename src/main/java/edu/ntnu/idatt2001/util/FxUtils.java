package edu.ntnu.idatt2001.util;

import java.io.File;
import java.util.Objects;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Window;

/**
 * A utility class for common JavaFX features.
 */
public final class FxUtils {

  /**
   * Opens a file chooser window and returns the file that was
   * chosen. Only files with the specified extensions are visible.
   *
   * @param   window the current window
   * @param   title the window title
   * @param   name the name of the files
   * @param   extensions the file extensions
   * @return  the chosen file
   */
  public static File fileChooser(Window window, String title, String name, String... extensions) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle(title);
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(name, extensions));
    return fileChooser.showOpenDialog(window);
  }

  /**
   * Returns an {@link ImageView} from a path with the given width and height.
   *
   * @param   path the image path
   * @param   fitWidth the width
   * @param   fitHeight the height
   * @return  an image view
   */
  public static ImageView image(String path, double fitWidth, double fitHeight) {
    Image image = new Image(Objects.requireNonNull(FxUtils.class.getResourceAsStream(path)));
    ImageView imageView = new ImageView(image);
    imageView.setFitWidth(fitWidth);
    imageView.setFitHeight(fitHeight);
    return imageView;
  }

  /**
   * Creates a new {@link Label} with the given text and font size.
   *
   * @param   text the text
   * @param   fontSize the font size
   * @return  a label
   */
  public static Label newLabel(String text, int fontSize) {
    Label label = new Label(text);
    label.setStyle("-fx-font-size: " + fontSize + ";");
    return newLabel(text, fontSize, Paint.valueOf("white"));
  }

  /**
   * Creates a new {@link Label} with the given text and font size.
   *
   * @param   text the text
   * @param   fontSize the font size
   * @param   paint the text color
   * @return  a label
   */
  public static Label newLabel(String text, int fontSize, Paint paint) {
    Label label = new Label(text);
    label.setFont(Font.font("System", fontSize));
    label.setTextFill(paint);
    return label;
  }

  /**
   * Creates a new {@link TextField} with no placeholder and
   * font size 12.
   *
   * @return a text field
   */
  public static TextField newTextField() {
    return newTextField(null, 12);
  }

  /**
   * Creates a new {@link TextField} with the given placeholder
   * and font size.
   *
   * @param   placeholder the placeholder text
   * @param   fontSize the font size
   * @return  a text field
   */
  public static TextField newTextField(String placeholder, int fontSize) {
    TextField field = new TextField(placeholder);
    field.setStyle("-fx-background-color: none; -fx-border-style: hidden hidden solid hidden; "
        + "-fx-border-width: 1px; -fx-border-color: white; -fx-padding: 0 0 4 0; -fx-font-size: "
        + fontSize + "; -fx-text-fill: white;");
    return field;
  }

  /**
   * Applies a stylesheet to a node.
   *
   * @param parent the parent to apply the stylesheet to
   * @param path the path of the stylesheet
   */
  public static void applyStyle(Parent parent, String path) {
    String s = FxUtils.class.getResource(path).toExternalForm();
    parent.getStylesheets().add(s);
  }

  /**
   * Removes all input that is not a number when applied to
   * a {@link TextInputControl}.
   *
   * @param inputControls the input controls
   */
  public static void forceNumericInput(TextInputControl... inputControls) {
    for (TextInputControl inputControl : inputControls) {
      inputControl.textProperty().addListener((observable, oldValue, newValue) -> {
        if (!newValue.matches("\\d*")) {
          inputControl.setText(newValue.replaceAll("[^\\d]", ""));
        }
      });
    }
  }
}
