package edu.ntnu.idatt2001.util;

/**
 * The paths to each button stylesheet.
 */
public enum ButtonStyle {

  BLUE("/style/blueButton.css"),
  BUTTON("/style/button.css"),
  GRAY("/style/grayButton.css");

  private final String path;

  ButtonStyle(String path) {
    this.path = path;
  }

  /**
   * Returns the path to the stylesheet.
   *
   * @return  the path to the stylesheet
   */
  public String getPath() {
    return path;
  }
}
