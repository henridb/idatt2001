package edu.ntnu.idatt2001.codec;

import edu.ntnu.idatt2001.file.Reader;
import edu.ntnu.idatt2001.file.Writer;
import edu.ntnu.idatt2001.model.game.Link;
import edu.ntnu.idatt2001.model.game.Passage;
import java.io.IOException;

/**
 * The {@code PassageCodec} class implements encoding and decoding
 * for {@link Passage} objects.
 */
public final class PassageCodec implements Codec<Passage> {

  /**
   * Decodes a passage from a reader.
   *
   * @param   reader the reader
   * @return  a passage
   */
  @Override
  public Passage decode(Reader reader) {
    Passage.Builder builder = new Passage.Builder();

    while (reader.hasNext(s -> s.contains(":") && !s.contains("["))) {
      String line = reader.read();
      String[] components = line.split(":");
      if (components.length != 2)
        throw new IllegalArgumentException("Invalid passage data on line " + reader.getCursor());
      String key = components[0];
      String value = components[1];
      switch (key) {
        case "title" -> builder.title(value);
        case "content" -> builder.content(value);
        case "image" -> builder.imagePath(value);
        case "time" -> builder.time(Integer.parseInt(value));
        default -> throw new IllegalArgumentException("unknown passage tag " + key);
      }
    }

    while (reader.hasNext(s -> s.matches("\\[[^]]*]\\([^)]*\\)\\{[^}]*\\}"))) {
      Link link = reader.read(Link.class);
      builder.addLink(link);
    }

    return builder.build();
  }

  /**
   * Encodes a passage to the writer.
   *
   * @param   writer the writer
   * @param   passage the passage to encode
   * @throws  IOException if an I/O error occurs
   */
  @Override
  public void encode(Writer writer, Passage passage) throws IOException {
    writer.writeLine("title:" + passage.getTitle());
    writer.writeLine("content:" + passage.getContent());
    if (passage.hasImage()) {
      writer.writeLine("image:" + passage.getImagePath());
    }
    if (passage.isTimed()) {
      writer.writeLine("time:" + passage.getTime());
    }
    for (Link link : passage.getLinks()) {
      writer.write(link);
    }
  }
}
