package edu.ntnu.idatt2001.codec;

import edu.ntnu.idatt2001.file.Reader;
import edu.ntnu.idatt2001.file.Writer;
import java.io.IOException;

/**
 * A {@code Codec} defines methods for encoding and decoding objects.
 *
 * @param   <T> the type of the object to encode and decode
 */
public interface Codec<T> {

  /**
   * Read an object from a reader.
   *
   * @param   reader the reader
   * @return  the object
   */
  T decode(Reader reader);

  /**
   * Write an object to a writer.
   *
   * @param   writer the writer
   * @param   value the value to write
   * @throws  IOException if an I/O error occurs
   */
  void encode(Writer writer, T value) throws IOException;
}
