package edu.ntnu.idatt2001.codec;

import edu.ntnu.idatt2001.file.Reader;
import edu.ntnu.idatt2001.file.Writer;
import edu.ntnu.idatt2001.model.action.*;
import edu.ntnu.idatt2001.model.game.Link;
import edu.ntnu.idatt2001.util.StringUtils;
import java.io.IOException;

/**
 * The {@code LinkCodec} class implements encoding and decoding
 * for {@link Link} objects.
 */
public final class LinkCodec implements Codec<Link> {

  /**
   * Decodes a link from a reader.
   *
   * @param   reader the reader
   * @return  a link
   */
  @Override
  public Link decode(Reader reader) {
    String s = reader.read();
    String text = s.substring(1, s.indexOf(']'));
    String reference = s.substring(s.indexOf('(') + 1, s.indexOf(')'));
    String actions = s.substring(s.indexOf('{') + 1, s.indexOf('}'));
    Link link = new Link(text, reference);
    for (String action : actions.split(",")) {
      String[] components = action.split(":");
      if (action.isBlank() || components.length != 2) {
        continue;
      }
      String value = components[1];
      switch (components[0]) {
        case "gold" -> link.addAction(new GoldAction(Integer.parseInt(value)));
        case "health" -> link.addAction(new HealthAction(Integer.parseInt(value)));
        case "inventory" -> link.addAction(new InventoryAction(value));
        case "score" -> link.addAction(new ScoreAction(Integer.parseInt(value)));
        default -> throw new IllegalArgumentException("invalid action type for link " + text);
      }
    }
    return link;
  }

  /**
   * Encodes a link to the writer.
   *
   * @param   writer the writer
   * @param   link the link to encode
   * @throws  IOException if an I/O error occurs
   */
  @Override
  public void encode(Writer writer, Link link) throws IOException {
    writer.write('[');
    writer.write(link.getText());
    writer.write(']');
    writer.write('(');
    writer.write(link.getReference());
    writer.write(')');
    writer.write("{");
    writer.write(StringUtils.join(",", link.getActions()));
    writer.write("}");
    writer.newLine();
  }
}
