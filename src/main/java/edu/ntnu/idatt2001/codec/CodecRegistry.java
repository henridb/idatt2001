package edu.ntnu.idatt2001.codec;

import edu.ntnu.idatt2001.model.game.Link;
import edu.ntnu.idatt2001.model.game.Passage;
import edu.ntnu.idatt2001.model.game.Story;
import java.util.HashMap;
import java.util.Map;

/**
 * The {@code CodecRegistry} manages the codecs used when reading and
 * writing from files.
 *
 * @see     Codec
 */
public final class CodecRegistry {

  private static final Map<Class<?>, Codec<?>> codecs = new HashMap<>();

  // Default codecs
  static {
    codecs.put(Story.class, new StoryCodec());
    codecs.put(Passage.class, new PassageCodec());
    codecs.put(Link.class, new LinkCodec());
  }

  /**
   * Returns the codec of a given class.
   *
   * @param   clazz the class
   * @param   <T> the type of the class
   * @return  the codec of a given class
   * @throws  IllegalArgumentException if no codec for the class is found
   */
  @SuppressWarnings("unchecked")
  public static <T> Codec<T> get(Class<T> clazz) {
    if (!codecs.containsKey(clazz)) {
      throw new IllegalArgumentException("no codec for type " + clazz.getSimpleName());
    }
    return (Codec<T>) codecs.get(clazz);
  }
}
