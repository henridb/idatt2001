package edu.ntnu.idatt2001.codec;

import edu.ntnu.idatt2001.file.Reader;
import edu.ntnu.idatt2001.file.Writer;
import edu.ntnu.idatt2001.model.game.Passage;
import edu.ntnu.idatt2001.model.game.Story;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@code StoryCodec} class implements encoding and decoding
 * for {@link Story} objects.
 */
public final class StoryCodec implements Codec<Story> {

  /**
   * Decodes a story from a reader.
   *
   * @param   reader the reader
   * @return  a story
   */
  @Override
  public Story decode(Reader reader) {
    String title = reader.read();
    reader.skipBlanks();
    List<Passage> passages = new ArrayList<>();
    while (reader.hasNext()) {
      Passage passage = reader.read(Passage.class);
      passages.add(passage);
      reader.skipBlanks();
    }
    Story story = new Story(title, passages.get(0));
    passages.forEach(story::addPassage);
    return story;
  }

  /**
   * Encodes a story to the writer.
   *
   * @param   writer the writer
   * @param   story the story to encode
   * @throws  IOException if an I/O error occurs
   */
  @Override
  public void encode(Writer writer, Story story) throws IOException {
    writer.writeLine(story.getTitle());
    writer.newLine();
    // As the passages are stored in a hashmap, the order cannot be guaranteed
    // upon retrieval, hence we must force the opening passage to be written first
    writer.write(story.getOpeningPassage());
    List<Passage> otherPassages = story.getPassages().stream()
        .filter(p -> !p.equals(story.getOpeningPassage())).toList();
    for (Passage passage : otherPassages) {
      writer.newLine();
      writer.write(passage);
    }
  }
}
