package edu.ntnu.idatt2001.page;

import edu.ntnu.idatt2001.Paths;
import edu.ntnu.idatt2001.util.ButtonBuilder;
import edu.ntnu.idatt2001.util.ButtonStyle;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

/**
 * The main menu, also known as the home page.
 */
public class HomePage extends MenuPage {

  /**
   * Instantiates a new home page.
   */
  public HomePage() {
    super("Paths");
    borderPane.setBackground(Paths.getBackground());

    VBox menuBox = new VBox();
    menuBox.getChildren().addAll(newGameButton(menuBox), instructionsButton(menuBox));
    menuBox.setAlignment(Pos.CENTER);
    menuBox.setSpacing(20);
    setCenter(menuBox);
  }

  /**
   * Returns a new game button.
   *
   * @param   parent the button parent node
   * @return  a button
   */
  private Button newGameButton(Node parent) {
    return new ButtonBuilder()
        .text("New game")
        .margin(parent, 20, 20, 20, 20)
        .style(ButtonStyle.BUTTON)
        .onClick(e -> Paths.setPage(new StoryPage()))
        .alignment(parent, Pos.CENTER)
        .build();
  }

  /**
   * Returns an instruction button.
   *
   * @param   parent the button parent node
   * @return  a button
   */
  private Button instructionsButton(Node parent) {
    return new ButtonBuilder()
        .text("Instructions")
        .margin(parent, 20, 20, 20, 20)
        .style(ButtonStyle.BUTTON)
        .onClick(e -> Paths.setPage(new InstructionsPage()))
        .alignment(parent, Pos.CENTER)
        .build();
  }
}
