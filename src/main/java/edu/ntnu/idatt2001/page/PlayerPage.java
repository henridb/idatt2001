package edu.ntnu.idatt2001.page;

import edu.ntnu.idatt2001.Paths;
import edu.ntnu.idatt2001.model.game.Player;
import edu.ntnu.idatt2001.util.ButtonBuilder;
import edu.ntnu.idatt2001.util.ButtonStyle;
import edu.ntnu.idatt2001.util.FxUtils;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;

/**
 * The page where you can set player name, health and gold.
 */
public class PlayerPage extends MenuPage {

  private static final String TOOLTIP = "You must insert a player name, health and gold.";

  public static Player player;


  /**
   * Instantiate a new player page.
   */
  public PlayerPage() {
    super("Create Player", TOOLTIP);
    borderPane.setBackground(Paths.getBackground());
    GridPane gridPane = new GridPane();
    gridPane.setVgap(10);
    gridPane.setHgap(10);
    gridPane.setAlignment(Pos.CENTER);

    Label nameLabel = FxUtils.newLabel("Name:", 16, Paint.valueOf("white"));
    TextField nameField = FxUtils.newTextField(player != null ? player.getName() : "", 16);

    Label healthLabel = FxUtils.newLabel("Health:", 16, Paint.valueOf("white"));
    TextField healthField = FxUtils.newTextField(
        String.valueOf(player != null ? player.getHealth() : 100), 16);

    Label goldLabel = FxUtils.newLabel("Gold:", 16, Paint.valueOf("white"));
    TextField goldField = FxUtils.newTextField(
        String.valueOf(player != null ? player.getGold() : 0), 16);

    FxUtils.forceNumericInput(healthField, goldField);

    gridPane.addColumn(0, nameLabel, healthLabel, goldLabel);
    gridPane.addColumn(1, nameField, healthField, goldField);

    StackPane resultPane = new StackPane();
    VBox centerBox = new VBox();
    Button continueButton = new ButtonBuilder()
        .text("Continue")
        .style(ButtonStyle.BUTTON)
        .margin(centerBox, 30)
        .onClick(event -> attemptContinue(resultPane, healthField.getText(),
            goldField.getText(), nameField.getText()))
        .build();

    centerBox.setAlignment(Pos.TOP_CENTER);
    centerBox.setPadding(new Insets(30, 60, 30, 60));
    centerBox.setMaxWidth(600);
    centerBox.getChildren().addAll(gridPane, resultPane, continueButton);
    setCenter(centerBox);
    setLeft(new ButtonBuilder()
        .text("Go back")
        .onClick(e -> Paths.setPage(new StoryPage()))
        .style(ButtonStyle.BUTTON)
        .build());
  }

  /**
   * Attempt to move to the next page.
   *
   * @param   resultPane the result pane
   * @param   health the health text
   * @param   gold the gold text
   * @param   name the name text
   */
  private void attemptContinue(StackPane resultPane, String health, String gold, String name) {
    try {
      player = new Player.Builder()
          .name(name)
          .health(Integer.parseInt(health.isBlank() ? "100" : health))
          .gold(Integer.parseInt(gold.isBlank() ? "0" : gold))
          .build();
      Paths.setPage(new GoalsPage());
    } catch (Exception e) {
      resultPane.getChildren().add(createErrorBox(resultPane, "Invalid input", e.getLocalizedMessage()));
    }
  }

  /**
   * Returns a copy of the player that was created. This is
   * to ensure that the initial state of the player is saved
   * between games.
   *
   * @return  the player
   */
  public static Player getPlayer() {
    return new Player(player);
  }
}
