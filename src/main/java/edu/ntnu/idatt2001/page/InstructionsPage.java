package edu.ntnu.idatt2001.page;

import edu.ntnu.idatt2001.Paths;
import edu.ntnu.idatt2001.util.ButtonBuilder;
import edu.ntnu.idatt2001.util.ButtonStyle;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * The page where you can view instructions.
 */
public class InstructionsPage extends MenuPage {

  /**
   * Instantiates a new instructions page.
   */
  public InstructionsPage() {
    super("How to play");

    setLeft(new ButtonBuilder()
        .text("Go back")
        .onClick(event -> Paths.setPage(new HomePage()))
        .style(ButtonStyle.GRAY)
        .build());

    ScrollPane scrollPane = new ScrollPane();
    scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
    scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
    Label startGame = new Label("Start a new game");
    startGame.setFont(Font.font("Courier New", FontWeight.BOLD, 14));
    startGame.setAlignment(Pos.TOP_LEFT);

    Text startGameText = new Text("""
                Start a game by loading a .paths file using the file chooser.
                The game will inform you of the number of passages and eventual broken links.
                If the file is in the wrong format, you will not be able to start the game.""");
    startGameText.setFont(Font.font("Courier New", 14));
    startGameText.wrappingWidthProperty().bind(scrollPane.widthProperty().subtract(20));

    Label createPlayer = new Label("Create a player");
    createPlayer.setFont(Font.font("Courier New", FontWeight.BOLD, 14));
    createPlayer.setAlignment(Pos.TOP_LEFT);

    Text createPlayerText = new Text("""
                You will need to create a player before you begin the game.
                Input the player name, health and gold.
                Health and gold cannot be lower than 0, and player name cannot be empty.
                Press 'Continue' when you are satisfied.""");
    createPlayerText.setFont(Font.font("Courier New", 14));
    createPlayerText.wrappingWidthProperty().bind(scrollPane.widthProperty().subtract(20));

    Label setGoals = new Label("Set goals");
    setGoals.setFont(Font.font("Courier New", FontWeight.BOLD, 14));

    Text setGoalsText = new Text("""
                Next you can set the goals for the game.
                Whenever any of the goals are fulfilled the game is over and you have won.
                Be careful not to set the goals to low as this makes the game easier.
                You do not have to set any goals if you do not want to.
                Press 'Start game' when you are satisfied.""");
    setGoalsText.setFont(Font.font("Courier New", 14));
    setGoalsText.wrappingWidthProperty().bind(scrollPane.widthProperty().subtract(20));

    Label playGame = new Label("Playing the game");
    playGame.setFont(Font.font("Courier New", FontWeight.BOLD, 14));

    Text playGameText = new Text("""
                The goal of the game is to choose the right passages to reach your goals or reach the end of the story.
                Make the wrong choice and the game could be over.
                All relevant information is displayed on the right side including your health, gold, score and inventory.""");
    playGameText.setFont(Font.font("Courier New", 14));
    playGameText.wrappingWidthProperty().bind(scrollPane.widthProperty().subtract(20));

    VBox instructionBox = new VBox();
    instructionBox.setSpacing(10);
    instructionBox.getChildren().addAll(startGame, startGameText,
        createPlayer, createPlayerText,
        setGoals, setGoalsText,
        playGame, playGameText);
    instructionBox.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, null, null)));
    instructionBox.setStyle("-fx-background-color: transparent");
    scrollPane.setContent(instructionBox);
    scrollPane.setStyle("-fx-background-color: transparent");
    setCenter(scrollPane);
  }

}
