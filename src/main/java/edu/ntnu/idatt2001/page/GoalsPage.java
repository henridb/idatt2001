package edu.ntnu.idatt2001.page;

import edu.ntnu.idatt2001.Paths;
import edu.ntnu.idatt2001.model.action.Action;
import edu.ntnu.idatt2001.model.action.InventoryAction;
import edu.ntnu.idatt2001.model.game.Link;
import edu.ntnu.idatt2001.model.game.Passage;
import edu.ntnu.idatt2001.model.goal.Goal;
import edu.ntnu.idatt2001.model.goal.GoldGoal;
import edu.ntnu.idatt2001.model.goal.InventoryGoal;
import edu.ntnu.idatt2001.model.goal.ScoreGoal;
import edu.ntnu.idatt2001.util.ButtonBuilder;
import edu.ntnu.idatt2001.util.ButtonStyle;
import edu.ntnu.idatt2001.util.FxUtils;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import org.controlsfx.control.CheckComboBox;

/**
 * The page where you can select goals.
 */
public class GoalsPage extends MenuPage {

  private static final String TOOLTIP = "Create goals for your game. You must complete"
      + "all goals to win.";

  private static final TextField goldGoalField = FxUtils.newTextField("", 16);
  private static final CheckComboBox<String> listOfItems = new CheckComboBox<>();
  private static final TextField scoreGoalField = FxUtils.newTextField("", 16);

  /**
   * Instantiates a new goals page.
   */
  public GoalsPage() {
    super("Set goals", TOOLTIP);
    borderPane.setBackground(Paths.getBackground());

    GridPane gridPane = new GridPane();
    gridPane.setVgap(10);
    gridPane.setHgap(10);
    gridPane.setAlignment(Pos.CENTER);

    ObservableList<String> items = FXCollections.observableArrayList();

    for (Passage passage : StoryPage.story.getPassages()) {
      for (Link link : passage.getLinks()) {
        for (Action action : link.getActions()) {
          if (action instanceof InventoryAction invAction) {
            items.add(invAction.getItem());
          }
        }
      }
    }
    listOfItems.getItems().addAll(items);

    Label scoreGoalLabel = FxUtils.newLabel("Score goal:", 16, Paint.valueOf("white"));
    Label goldGoalLabel = FxUtils.newLabel("Gold goal:", 16, Paint.valueOf("white"));
    Label invGoalLabel = FxUtils.newLabel("Inventory goal:", 16, Paint.valueOf("white"));

    FxUtils.forceNumericInput(goldGoalField, scoreGoalField);

    gridPane.addColumn(0, goldGoalLabel, scoreGoalLabel, invGoalLabel);
    gridPane.addColumn(1, goldGoalField, scoreGoalField, listOfItems);

    VBox centerBox = new VBox();
    centerBox.setAlignment(Pos.TOP_CENTER);
    centerBox.setPadding(new Insets(30, 60, 30, 60));
    centerBox.setMaxWidth(600);
    StackPane resultPane = new StackPane();
    Button startGameButton = new ButtonBuilder()
        .text("Start game")
        .style(ButtonStyle.BUTTON)
        .margin(centerBox, 30)
        .onClick(event -> attemptContinue(resultPane))
        .build();

    centerBox.getChildren().addAll(gridPane, resultPane, startGameButton);
    setCenter(centerBox);
    setLeft(new ButtonBuilder()
        .text("Go back")
        .style(ButtonStyle.BUTTON)
        .onClick(e -> Paths.setPage(new PlayerPage()))
        .build());
  }

  /**
   * Attempts to continue to the next page.
   *
   * @param   resultPane the result pane
   */
  private void attemptContinue(StackPane resultPane) {
    try {
      getGoals(); // Catch errors
      Paths.setPage(new GamePage());
    } catch (Exception e) {
      resultPane.getChildren().add(createErrorBox(resultPane, "Invalid input", e.getLocalizedMessage()));
    }
  }

  /**
   * Returns a list of the current goals set.
   *
   * @return  a list of the current goals set
   */
  public static List<Goal> getGoals() {
    List<Goal> goals = new ArrayList<>();

    // Get gold goal
    String gold = goldGoalField.getText();
    if (gold != null && !gold.isBlank()) {
      goals.add(new GoldGoal(Integer.parseInt(gold)));
    }

    // Get score goal
    String score = scoreGoalField.getText();
    if (score != null && !score.isBlank()) {
      goals.add(new ScoreGoal(Integer.parseInt(score)));
    }

    // Get inventory goal
    List<String> items = new ArrayList<>();
    if (listOfItems.getItems() != null) {
      for (String s : listOfItems.getItems()) {
        if (listOfItems.getItemBooleanProperty(s).get()) {
          items.add(s);
        }
      }
    }
    if (!items.isEmpty()) {
      goals.add(new InventoryGoal(items));
    }

    return goals;
  }
}
