package edu.ntnu.idatt2001.page;

import edu.ntnu.idatt2001.Paths;
import edu.ntnu.idatt2001.util.ButtonBuilder;
import edu.ntnu.idatt2001.util.FxUtils;
import java.util.Objects;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

/**
 * This class is a parent class for all menu pages. This is to
 * ensure similar layout between pages.
 */
public abstract class MenuPage extends Page {

  protected final BorderPane borderPane = new BorderPane();
  private final String title;
  private final String tooltip;

  protected Stage stage;

  /**
   * Instantiates a menu page without a tooltip.
   *
   * @param   title the page title
   * @throws  NullPointerException if {@code title} is null
   */
  protected MenuPage(String title) {
    this(title, null);
  }

  /**
   * Instantiates a menu page.
   *
   * @param   title the page title
   * @param   tooltip the page tooltip
   * @throws  NullPointerException if {@code title} is null
   */
  protected MenuPage(String title, String tooltip) {
    this.title = Objects.requireNonNull(title, "title must not be null");
    this.tooltip = tooltip;
  }

  /**
   * Set the center property of the border pane.
   *
   * @param   node the node
   */
  protected final void setCenter(Node node) {
    if (node == null) return;
    borderPane.setCenter(node);
  }

  /**
   * Set the left property of the border pane.
   *
   * @param   node the node
   */
  protected final void setLeft(Node node) {
    if (node == null) return;
    borderPane.setLeft(node);
    BorderPane.setMargin(node, new Insets(20));
  }

  /**
   * Set the right property of the border pane.
   *
   * @param   node the node
   */
  protected final void setRight(Node node) {
    if (node == null) return;
    borderPane.setRight(node);
  }

  /**
   * Loads the page.
   *
   * @param   stage the JavaFX stage
   */
  @Override
  public final void load(Stage stage) {
    this.stage = stage;
    borderPane.setTop(createTitle());
    if (tooltip != null) {
      Button tooltip = createTooltipButton();
      borderPane.setRight(tooltip);
      BorderPane.setAlignment(tooltip, Pos.TOP_RIGHT);
      BorderPane.setMargin(tooltip, new Insets(20));
    }

    Scene scene = new Scene(borderPane, Paths.HEIGHT, Paths.WIDTH);
    stage.setScene(scene);
  }

  /**
   * Create a generic error box.
   *
   * @param   parent the parent node
   * @param   title the error title
   * @param   message the error message
   * @return  an hBox containing the error
   */
  protected final HBox createErrorBox(Node parent, String title, String message) {
    HBox errorHbox = new HBox();
    if (parent instanceof StackPane) {
      StackPane.setMargin(errorHbox, new Insets(20, 0, 20, 0));
    } else if (parent instanceof GridPane) {
      GridPane.setMargin(errorHbox, new Insets(20, 0, 20, 0));
    }
    VBox errorVbox = new VBox();
    errorVbox.setPadding(new Insets(0, 0, 0, 9));
    Label errorTitle = new Label(title);
    errorTitle.setWrapText(true);
    errorTitle.getStyleClass().add("errorTitle");
    Label error = new Label(message);
    error.setWrapText(true);
    errorVbox.getChildren().addAll(errorTitle, error);
    FxUtils.applyStyle(errorHbox, "/style/errorBox.css");
    errorHbox.setStyle("-fx-max-height: 500px;");
    errorHbox.getStyleClass().add("errorBox");
    ImageView imageView = FxUtils.image("/images/error.png", 22, 22);
    errorHbox.getChildren().addAll(imageView, errorVbox);
    return errorHbox;
  }

  /**
   * Creates a menu page  title.
   *
   * @return  a label with the title
   */
  private Label createTitle() {
    Label titleLabel = new Label(title);
    BorderPane.setAlignment(titleLabel, Pos.CENTER);
    titleLabel.setPadding(new Insets(20, 20, 20, 20));
    titleLabel.setStyle("-fx-font-size: 32px; -fx-font-weight: bold;"
        + "-fx-font-family: 'Courier New'");
    if (!(this instanceof GamePage) && !(this instanceof InstructionsPage)) {
      titleLabel.setTextFill(Paint.valueOf("white"));
    }

    return titleLabel;
  }

  /**
   * Creates a tooltip button.
   *
   * @return  a tooltip button
   */
  private Button createTooltipButton() {
    Button button = new ButtonBuilder()
        .child(FxUtils.image("/images/help.png", 22, 22))
        .margin(borderPane, 10)
        .alignment(borderPane, Pos.TOP_RIGHT)
        .build();
    button.setStyle("-fx-background-color: transparent; -fx-cursor: hand; -fx-font-size: 18px;"
        + "-fx-padding: 10px;");
    button.setTooltip(new Tooltip(tooltip));
    return button;
  }
}
