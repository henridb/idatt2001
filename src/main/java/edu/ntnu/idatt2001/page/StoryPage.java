package edu.ntnu.idatt2001.page;

import edu.ntnu.idatt2001.Paths;
import edu.ntnu.idatt2001.file.FileManager;
import edu.ntnu.idatt2001.model.game.Story;
import edu.ntnu.idatt2001.util.ButtonBuilder;
import edu.ntnu.idatt2001.util.ButtonStyle;
import edu.ntnu.idatt2001.util.FxUtils;
import java.io.File;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.stage.Window;

/**
 * The page where you select a story to play.
 */
public class StoryPage extends MenuPage {

  private static final String TOOLTIP = "Select the story you want to play. You will see the"
      + "the file path to the selected story once you have selected one. You can only play files that"
      + "end with .paths.";

  public static File storyFile;
  public static Story story;

  private final VBox centerBox = new VBox();
  private final Button continueButton = continueButton();
  private final StackPane resultPane = new StackPane();
  private final Label filePath = new Label("No file chosen");

  /**
   * Instantiate a new story page.
   */
  public StoryPage() {
    super("Select Story", TOOLTIP);
    borderPane.setBackground(Paths.getBackground());
    loadFile(storyFile);
    HBox fileBox = new HBox();
    fileBox.setAlignment(Pos.CENTER);
    VBox.setMargin(fileBox, new Insets(30, 0, 30, 0));
    filePath.setWrapText(true);
    filePath.setStyle("-fx-font-size: 16px; -fx-text-fill: white;");

    fileBox.getChildren().addAll(fileButton(fileBox), filePath);

    centerBox.setAlignment(Pos.TOP_CENTER);
    centerBox.setPadding(new Insets(30, 60, 30, 60));
    centerBox.setMaxWidth(600);
    centerBox.getChildren().addAll(fileBox, resultPane, continueButton);
    setCenter(centerBox);
    setLeft(new ButtonBuilder()
        .text("Go back")
        .onClick(e -> Paths.setPage(new HomePage()))
        .style(ButtonStyle.BUTTON)
        .build());
  }

  /**
   * Creates a file chooser button.
   *
   * @param   parent the button parent node
   * @return  a button
   */
  private Button fileButton(Node parent) {
    return new ButtonBuilder()
        .text("Choose file...")
        .margin(parent, 0, 15, 0, 0)
        .style(ButtonStyle.BUTTON)
        .onClick(e -> findStoryFile(stage))
        .alignment(parent, Pos.CENTER)
        .build();
  }

  /**
   * Opens a file chooser.
   *
   * @param   window the current window
   */
  private void findStoryFile(Window window) {
    storyFile = FxUtils.fileChooser(window, "Open Paths File", "Story files (*.paths)", "*.paths");
    loadFile(storyFile);
  }

  /**
   * Attempts to load the file.
   *
   * @param   file the file to load
   */
  private void loadFile(File file) {
    if (file == null) return;
    loadStory(file);
    filePath.setText(file.getPath());
  }

  /**
   * Returns the continue button.
   *
   * @return  a button
   */
  private Button continueButton() {
    return new ButtonBuilder()
        .text("Continue")
        .style(ButtonStyle.BUTTON)
        .onClick(e -> Paths.setPage(new PlayerPage()))
        .disabled(true)
        .margin(centerBox, 30)
        .build();
  }

  /**
   * Loads the selected story.
   *
   * @param file the story file
   */
  private void loadStory(File file) {
    resultPane.getChildren().clear();
    try {
      story = FileManager.readStory(file);
      HBox storyBox = new HBox();
      storyBox.setStyle("-fx-font-size: 14px");
      storyBox.setAlignment(Pos.CENTER);
      VBox titleBox = new VBox();
      titleBox.setPadding(new Insets(0, 30, 0, 0));
      Label title = FxUtils.newLabel("Story", 18, Paint.valueOf("white"));
      Label titleName = new Label(story.getTitle());
      titleName.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: white;");
      titleBox.getChildren().addAll(title, titleName);
      VBox passagesBox = new VBox();
      Label passages = FxUtils.newLabel("Passages", 18, Paint.valueOf("white"));
      Label passagesName = new Label(String.valueOf(story.getPassages().size()));
      passagesName.setStyle("-fx-font-size: 18px; -fx-font-weight: bold; -fx-text-fill: white;");
      passagesBox.getChildren().addAll(passages, passagesName);
      storyBox.getChildren().addAll(titleBox, passagesBox);
      resultPane.getChildren().add(storyBox);
      continueButton.setDisable(false);
    } catch (Exception e) {
      continueButton.setDisable(true);
      HBox errorHbox = createErrorBox(resultPane, "Failed to load story", e.getLocalizedMessage());
      resultPane.getChildren().add(errorHbox);
    }
  }
}
