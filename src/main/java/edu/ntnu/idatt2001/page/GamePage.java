package edu.ntnu.idatt2001.page;

import edu.ntnu.idatt2001.Paths;
import edu.ntnu.idatt2001.model.action.Action;
import edu.ntnu.idatt2001.model.game.Game;
import edu.ntnu.idatt2001.model.game.Link;
import edu.ntnu.idatt2001.model.game.Passage;
import edu.ntnu.idatt2001.model.goal.*;
import edu.ntnu.idatt2001.util.ButtonBuilder;
import edu.ntnu.idatt2001.util.ButtonStyle;
import edu.ntnu.idatt2001.util.FxUtils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.TextAlignment;

/**
 * This is the page where the game is running.
 */
public class GamePage extends MenuPage {

  private final VBox contentBox = new VBox();
  private final Label timeLabel = new Label();
  private final Label scoreLabel = new Label();
  private final Label goldLabel = new Label();
  private final Label healthLabel = new Label();
  private final GridPane linkPane = new GridPane();
  private final VBox invContent = new VBox();

  private Game game;
  private ScheduledThreadPoolExecutor executor;
  private ScheduledFuture<?> future;

  /**
   * Instantiate a new game page.
   */
  public GamePage() {
    super(StoryPage.story.getTitle());

    contentBox.setAlignment(Pos.TOP_CENTER);

    linkPane.setMaxWidth(700);
    linkPane.setHgap(40);
    linkPane.setVgap(40);
    linkPane.setAlignment(Pos.TOP_CENTER);
    linkPane.setPadding(new Insets(30));

    reset();
  }

  /**
   * Resets the game to its initial state.
   */
  private void reset() {
    this.game = new Game(PlayerPage.getPlayer(), StoryPage.story, new ArrayList<>());
    for (Goal goal : GoalsPage.getGoals()) {
      this.game.addGoal(goal);
    }
    executor = new ScheduledThreadPoolExecutor(1, r -> {
      Thread thread = new Thread(r);
      thread.setDaemon(true); // Close threads after application shutdown
      return thread;
    });
    VBox centerBox = new VBox();
    centerBox.getChildren().addAll(contentBox, linkPane);
    centerBox.setAlignment(Pos.TOP_CENTER);
    setCenter(centerBox);
    setLeft(getLeft());
    updatePassage(game.begin());
  }

  private void updatePassage(Passage passage) {
    Label passageText = new Label(passage.getTitle());
    passageText.setStyle("-fx-font-size: 18px;");
    Label contentText = new Label(passage.getContent().replace("%name%",
        game.getPlayer().getName()));
    contentText.setStyle("-fx-font-size: 16px; -fx-max-width: 500px;");
    contentText.setWrapText(true);
    contentText.setTextAlignment(TextAlignment.CENTER);
    VBox textBox = new VBox();
    textBox.setMinWidth(300);
    textBox.setMinHeight(90);
    textBox.setAlignment(Pos.TOP_CENTER);
    textBox.getChildren().addAll(passageText, contentText);
    setRight(getGameInfoNode());

    contentBox.getChildren().clear();
    contentBox.getChildren().add(textBox);

    if (passage.hasImage()) {
      displayImage(passage);
    }

    linkPane.getChildren().clear();

    if (future != null && !future.isCancelled()) {
      future.cancel(true);
    }

    timeLabel.setTextFill(Paint.valueOf("black"));
    if (passage.isTimed()) {
      runPassageTimer(passage);
    } else {
      timeLabel.setText("Not timed");
    }

    // Update game info
    scoreLabel.setText(String.valueOf(game.getPlayer().getScore()));
    goldLabel.setText(String.valueOf(game.getPlayer().getGold()));
    healthLabel.setText(String.valueOf(game.getPlayer().getHealth()));
    updateInventory();

    if (checkEnd(passage)) {
      return;
    }

    // Display links
    int columns = Math.min(passage.getLinks().size(), 3);
    for (int i = 0; i < passage.getLinks().size(); i++) {
      linkPane.add(createOption(passage.getLinks().get(i)), i % columns, i / columns);
    }
  }

  /**
   * Displays the passage image.
   *
   * @param   passage the passage
   */
  private void displayImage(Passage passage) {
    File parentFile = StoryPage.storyFile.getParentFile();
    try {
      Image image = new Image(Objects.requireNonNull(
          new File(parentFile + passage.getImagePath()).toURI()
              .toURL().openStream()));
      ImageView imageView = new ImageView(image);
      imageView.setFitWidth(400);
      imageView.setFitHeight(250);
      contentBox.getChildren().add(imageView);
    } catch (IOException e) {
      // Ignore invalid image
    }
  }

  /**
   * Runs a timer for the passage.
   *
   * @param   passage the passage
   */
  private void runPassageTimer(Passage passage) {
    timeLabel.setText(String.valueOf(passage.getTime()));
    future = executor.scheduleAtFixedRate(() -> {
      Platform.runLater(() -> {
        int cur = Integer.parseInt(timeLabel.getText()) - 1;
        timeLabel.setText(String.valueOf(cur));
        if (cur < 11) {
          timeLabel.setTextFill(Paint.valueOf("red"));
        }
        if (cur <= 0) {
          showGameOver("GAME OVER", "You ran out of time");
        }
      });
    }, 1, 1, TimeUnit.SECONDS);
  }

  /**
   * Checks if the game has ended.
   *
   * @param   passage the current passage
   * @return  true if the game has ended, otherwise false
   */
  private boolean checkEnd(Passage passage) {
    if (game.getPlayer().getHealth() <= 0) {
      showGameOver("GAME OVER", "You ran out of health");
      return true;
    }

    if (!game.getGoals().isEmpty()
        && game.getGoals().stream().allMatch(goal -> goal.isFulfilled(game.getPlayer()))) {
      showGameOver("YOU WON", "You reached all of your goals");
      return true;
    }

    if (passage.getLinks().isEmpty()) {
      showGameOver("THE END", "You reached the end of the story");
      return true;
    }
    return false;
  }

  /**
   * Display game over box.
   *
   * @param   title the title of the box
   * @param   content the reason for the game end
   */
  private void showGameOver(String title, String content) {
    executor.shutdownNow();
    VBox gameOverBox = new VBox();
    gameOverBox.setAlignment(Pos.CENTER);
    gameOverBox.getChildren().add(FxUtils.newLabel(title, 20, Paint.valueOf("black")));
    gameOverBox.getChildren().add(FxUtils.newLabel(content, 18, Paint.valueOf("black")));
    HBox buttonBox = new HBox();
    buttonBox.setAlignment(Pos.CENTER);
    buttonBox.getChildren().addAll(tryAgainButton(buttonBox), mainMenuButton(buttonBox));
    gameOverBox.getChildren().add(buttonBox);
    VBox centerBox = ((VBox) borderPane.getCenter());
    linkPane.getChildren().clear();
    centerBox.getChildren().add(gameOverBox);
    setLeft(null);
  }

  /**
   * Creates a try again button.
   *
   * @param   parent the button parent node
   * @return  a button
   */
  private Button tryAgainButton(Node parent) {
    return new ButtonBuilder()
        .text("Try again")
        .style(ButtonStyle.BLUE)
        .alignment(parent, Pos.CENTER)
        .margin(parent, 20)
        .onClick(e -> reset())
        .build();
  }

  /**
   * Creates a main menu button.
   *
   * @param   parent the button parent node
   * @return  a button
   */
  private Button mainMenuButton(Node parent) {
    return new ButtonBuilder()
        .text("Main menu")
        .style(ButtonStyle.BLUE)
        .alignment(parent, Pos.CENTER)
        .margin(parent, 20)
        .onClick(e -> Paths.setPage(new HomePage()))
        .build();
  }

  /**
   * Updates the inventory item display.
   */
  private void updateInventory() {
    invContent.getChildren().clear();
    if (game.getPlayer().getInventory().isEmpty()) {
      Label label = new Label("No items");
      label.setStyle("-fx-font-size: 16px;");
      invContent.getChildren().add(label);
      return;
    }
    for (String item : game.getPlayer().getInventory()) {
      Label label = new Label(item);
      label.setStyle("-fx-font-size: 16px;");
      invContent.getChildren().add(label);
    }
  }

  /**
   * Create a link button.
   *
   * @param   link the link to create a button for
   * @return  a label
   */
  private Label createOption(Link link) {
    Label label = new Label(link.getText());
    label.setStyle("-fx-background-color: #2675b5; -fx-text-fill: white; -fx-font-size: 16px;"
        + "-fx-border-radius: 3px; -fx-cursor: hand; -fx-max-width: 200;");
    label.setPadding(new Insets(15));
    label.setWrapText(true);
    label.setAlignment(Pos.CENTER);
    label.setOnMouseClicked(e -> {
      for (Action action : link.getActions()) {
        action.execute(game.getPlayer());
      }
      for (Passage passage : game.getStory().getPassages()) {
        if (passage.getTitle().equals(link.getReference())) {
          updatePassage(passage);
        }
      }
    });
    return label;
  }

  /**
   * Returns an exit button for the left property
   * of the border pane.
   *
   * @return  a vBox
   */
  private VBox getLeft() {
    VBox leftBox = new VBox();
    leftBox.getChildren().addAll(new ButtonBuilder().text("Exit")
        .onClick(e -> Paths.setPage(new HomePage()))
        .style(ButtonStyle.BLUE).margin(leftBox, 10).build());
    return leftBox;
  }

  /**
   * Display various game information.
   *
   * @return  a vBox containing game information
   */
  private VBox getGameInfoNode() {
    Label inventoryTitle = new Label("Inventory");
    inventoryTitle.setStyle("-fx-font-size: 18px; -fx-padding: 0 0 0 10;");
    HBox inventoryBox = new HBox();
    inventoryBox.getChildren().addAll(FxUtils.image("/images/backpack.png",
        21, 21), inventoryTitle);
    HBox timeBox = new HBox();
    timeBox.getChildren().addAll(FxUtils.image("/images/timer.png", 21, 21), timeLabel);
    timeLabel.setStyle("-fx-font-size: 18px; -fx-padding: 0 0 0 10;");
    HBox scoreBox = new HBox();
    scoreBox.getChildren().addAll(FxUtils.image("/images/score.png",
        21, 21), scoreLabel);
    scoreLabel.setStyle("-fx-font-size: 18px; -fx-padding: 0 0 0 10;");
    HBox goldBox = new HBox();
    goldBox.getChildren().addAll(FxUtils.image("/images/gold.png",
        21, 21), goldLabel);
    goldLabel.setStyle("-fx-font-size: 18px; -fx-padding: 0 0 0 10;");
    HBox healthBox = new HBox();
    healthBox.getChildren().addAll(FxUtils.image("/images/heart.png",
        21, 21), healthLabel);
    healthLabel.setStyle("-fx-font-size: 18px; -fx-padding: 0 0 0 10;");
    ScrollPane inventoryPane = new ScrollPane();
    inventoryPane.setContent(invContent);
    inventoryPane.setFitToWidth(true);
    inventoryPane.setStyle("-fx-background-color: none;");
    VBox infoBox = new VBox();
    infoBox.setPadding(new Insets(30));
    infoBox.getChildren().addAll(timeBox, scoreBox, goldBox, healthBox,
        inventoryBox, inventoryPane, createGoalsBox());
    return infoBox;
  }

  /**
   * Creates a vBox with game goals.
   *
   * @return a vBox
   */
  private VBox createGoalsBox() {
    VBox goalBox = new VBox();
    goalBox.getChildren().add(FxUtils.newLabel("Goals",
        14, Paint.valueOf("black")));
    if (!game.getGoals().isEmpty()) {
      for (Goal goal : game.getGoals()) {
        String name = "";
        if (goal instanceof GoldGoal) {
          name = "Gold";
        } else if (goal instanceof HealthGoal) {
          name = "Health";
        } else if (goal instanceof ScoreGoal) {
          name = "Score";
        } else if (goal instanceof InventoryGoal) {
          name = "Items";
        }
        String status = goal.isFulfilled(game.getPlayer()) ? "Completed" : "Not completed";
        goalBox.getChildren().add(FxUtils.newLabel(name + ": " + status,
            14, Paint.valueOf("black")));
      }
    }
    return goalBox;
  }
}
