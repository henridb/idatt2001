package edu.ntnu.idatt2001.page;

import javafx.stage.Stage;

/**
 * A {@code Page} represents a JavaFX stage.
 */
public abstract class Page {

  /**
   * Loads a new page by passing the stage.
   *
   * @param   stage the stage
   */
  public abstract void load(Stage stage);
}
