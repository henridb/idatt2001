package edu.ntnu.idatt2001.file;

import edu.ntnu.idatt2001.codec.Codec;
import edu.ntnu.idatt2001.codec.CodecRegistry;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.function.Function;

/**
 * The {@code Reader} class acts as a wrapper around a scanner object and provides useful
 * methods to read data line-by-line from an input stream. It saves the source data
 * in a list to allow the reader to go to previous lines if needed.
 */
public final class Reader {

  private final List<String> lines = new ArrayList<>();
  private int cursor; // The position of the reader

  /**
   * Instantiates a new reader from an input stream source.
   *
   * @param   source the input stream
   * @throws  NullPointerException if {@code source} is null
   * @throws  IllegalArgumentException if the source is empty
   */
  public Reader(InputStream source) {
    Objects.requireNonNull(source, "source must not be null");
    Scanner scanner = new Scanner(source, StandardCharsets.UTF_8);
    if (!scanner.hasNext()) {
      throw new IllegalArgumentException("file must not be empty");
    }
    while (scanner.hasNext()) {
      lines.add(scanner.nextLine());
    }
  }

  /**
   * Read the next chunk of lines as the specified
   * class. It is required to register a {@link Codec} before
   * using this method.
   *
   * @param   clazz the class of the object
   * @param   <T> the type of the object to read
   * @return  the object with the given class type
   * @see     Codec
   */
  public <T> T read(Class<T> clazz) {
    return CodecRegistry.get(clazz).decode(this);
  }

  /**
   * Read the next line.
   *
   * @return  the next line
   */
  public String read() {
    return lines.get(cursor++);
  }

  /**
   * Advance past all blank lines.
   */
  public void skipBlanks() {
    while (hasNext()) {
      String line = lines.get(cursor);
      if (!line.isBlank()) {
        break;
      }
      cursor++;
    }
  }

  /**
   * Returns the current cursor.
   *
   * @return  the current cursor
   */
  public int getCursor() {
    return cursor;
  }

  /**
   * Returns true if the reader has more lines.
   *
   * @return  true if the reader has more lines, otherwise false
   */
  public boolean hasNext() {
    return cursor < lines.size();
  }

  /**
   * If there are more lines in the reader, this method
   * applies the given function to the current line and
   * returns true if the line is matching.
   *
   * @param   matcher the matcher
   * @return  true if there are more lines to read and
   *          the next line is matching the {@code matcher}
   */
  public boolean hasNext(Function<String, Boolean> matcher) {
    return hasNext() && matcher.apply(lines.get(cursor));
  }
}