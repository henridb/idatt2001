package edu.ntnu.idatt2001.file;

import edu.ntnu.idatt2001.codec.Codec;
import edu.ntnu.idatt2001.codec.CodecRegistry;
import java.io.*;
import java.util.Objects;

/**
 * The {@code Writer} class acts as a wrapper around
 * a {@link BufferedWriter} object and provides useful
 * methods to write data.
 */
public final class Writer implements Closeable {

  private final BufferedWriter writer;

  /**
   * Instantiates a new writer.
   *
   * @param   file the file to write to
   * @throws  NullPointerException if {@code file} is {@code null}
   * @throws  IOException if an I/O error occurs
   */
  public Writer(File file) throws IOException {
    Objects.requireNonNull(file, "file must not be null");
    FileManager.checkExtension(file);
    writer = new BufferedWriter(new FileWriter(file));
  }

  /**
   * Write a new line.
   *
   * @throws  IOException if an I/O error occurs
   */
  public void newLine() throws IOException {
    writer.newLine();
  }

  /**
   * Write text and end with a new line.
   *
   * @param   string the line text
   * @throws  IOException if an I/O error occurs
   */
  public void writeLine(String string) throws IOException {
    writer.write(string);
    writer.newLine();
  }

  /**
   * Write a character.
   *
   * @param   c the character
   * @throws  IOException if an I/O error occurs
   */
  public void write(char c) throws IOException {
    writer.write(c);
  }

  /**
   * Write a string.
   *
   * @param   string the string
   * @throws  IOException if an I/O error occurs
   */
  public void write(String string) throws IOException {
    writer.write(string);
  }

  /**
   * Write an object. It is required to register a {@link Codec}
   * before using this method.
   *
   * @param   object the object to write
   * @param   <T> the type of the object
   * @throws  IOException if an I/O error occurs
   */
  public <T> void write(T object) throws IOException {
    Codec<T> codec = (Codec<T>) CodecRegistry.get(object.getClass());
    codec.encode(this, object);
  }

  /**
   * Close the writer.
   *
   * @throws  IOException if an I/O error occurs
   */
  @Override
  public void close() throws IOException {
    writer.flush();
    writer.close();
  }
}