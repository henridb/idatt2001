package edu.ntnu.idatt2001.file;

import edu.ntnu.idatt2001.model.game.Link;
import edu.ntnu.idatt2001.model.game.Story;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * This class provides useful methods for writing and reading from files.
 */
public final class FileManager {

  /**
   * Reads a story from a file. A Story has the following format:
   *
   * <pre>
   * Story Title                              The story title
   *                                          At least one blank line separating
   * title:Passage 1                          The passage title
   * content:This is passage 1                The passage content
   * image:path/to/image.png                  The path to the image to display
   * [Link 1](Passage 2){score:10,health:-20} The link name, reference and actions
   * [Link 2](Passage X){score:100}
   *
   * title:Passage 2
   * content:This is passage 2
   * time:40
   * </pre>
   * The {@code image} and {@code time} tags are optional.
   *
   * @param   file the file containing the story
   * @return  a story
   * @throws  IllegalArgumentException if the file extension is wrong
   */
  public static Story readStory(File file) {
    try {
      checkExtension(file);
      Story story = fromFile(file, Story.class);
      checkBrokenLinks(story);
      return story;
    } catch (IOException e) {
      throw new IllegalStateException(e.getLocalizedMessage());
    }
  }

  /**
   * Throws an exception if any broken links are found.
   *
   * @param   story the story
   * @throws  NullPointerException if {@code story} is null
   * @throws  IllegalArgumentException if a broken link is found
   */
  public static void checkBrokenLinks(Story story) {
    Objects.requireNonNull(story, "story must not be null");
    for (Link link : story.getBrokenLinks()) {
      throw new IllegalArgumentException("Invalid link reference '" + link.getReference()
          + "' for link " + link.getText()); // Throw only the first broken link
    }
  }

  /**
   * Throws an exception if the file extension is not {@code .paths}.
   *
   * @param   file the file
   * @throws  IllegalArgumentException if the extension is not {@code .paths}
   */
  public static void checkExtension(File file) {
    String path = file.getPath();
    if (!path.substring(path.lastIndexOf('.')).equals(".paths")) {
      throw new IllegalArgumentException("file must have .paths extension");
    }
  }

  /**
   * Writes an object to a file using {@link Writer}.
   *
   * @param   object the object to write
   * @param   file the file to write to
   * @throws  IOException if an I/O error occurs
   */
  public static void toFile(Object object, File file) throws IOException {
    try (Writer writer = new Writer(file)) {
      writer.write(object);
    }
  }

  /**
   * Reads an object of the given class from a file.
   *
   * @param   file the file
   * @param   clazz the object class
   * @param   <T> the type of the object
   * @return  an object of the given class
   * @throws  IOException if an I/O error occurs
   */
  private static <T> T fromFile(File file, Class<T> clazz) throws IOException {
    return fromReader(new Reader(file.toURI().toURL().openStream()), clazz);
  }

  /**
   * Reads an object of the given class from a reader.
   *
   * @param   reader the reader
   * @param   clazz the object class
   * @param   <T> the type of the object
   * @return  an object of the given class
   */
  private static <T> T fromReader(Reader reader, Class<T> clazz) {
    return reader.read(clazz);
  }
}
