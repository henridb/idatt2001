package edu.ntnu.idatt2001.model.goal;

import edu.ntnu.idatt2001.model.game.Player;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

/**
 * A {@code InventoryGoal} is fulfilled when a player has
 * obtained a certain item.
 */
public final class InventoryGoal implements Goal {

  private final List<String> items;

  /**
   * Instantiates a new inventory goal.
   *
   * @param   items the mandatory items
   * @throws  NullPointerException if {@code items} is {@code null}
   */
  public InventoryGoal(List<String> items) {
    Objects.requireNonNull(items, "items must not be null");
    this.items = items;
  }

  /**
   * Returns true if the player has the items.
   *
   * @param   player the player
   * @return  true if the player has the items
   */
  @Override
  public boolean isFulfilled(Player player) {
    // Wrapping in HashSet for performance
    return new HashSet<>(player.getInventory()).containsAll(items);
  }
}