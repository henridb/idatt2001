package edu.ntnu.idatt2001.model.goal;

import edu.ntnu.idatt2001.model.game.Player;

/**
 * A {@code HealthGoal} is fulfilled when a player has
 * reached a certain amount of health.
 */
public final class HealthGoal implements Goal {

  private final int health;

  /**
   * Instantiates a new health goal.
   *
   * @param   health the minimum health necessary
   * @throws  IllegalArgumentException if {@code health} is less than 1
   */
  public HealthGoal(int health) {
    if (health < 1) {
      throw new IllegalArgumentException("health must be more than 0");
    }
    this.health = health;
  }

  /**
   * Returns true if the player has the minimum health necessary.
   *
   * @param   player the player
   * @return  true if the player has the minimum health necessary
   */
  @Override
  public boolean isFulfilled(Player player) {
    return player.getHealth() >= health;
  }
}