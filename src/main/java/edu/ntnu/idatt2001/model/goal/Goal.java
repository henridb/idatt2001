package edu.ntnu.idatt2001.model.goal;

import edu.ntnu.idatt2001.model.game.Player;

/**
 * A {@code Goal} represents a desired game state.
 */
public interface Goal {

  /**
   * Returns true if the goal is fulfilled.
   *
   * @param   player the player
   * @return  true if this goal is fulfilled, otherwise false
   */
  boolean isFulfilled(Player player);
}