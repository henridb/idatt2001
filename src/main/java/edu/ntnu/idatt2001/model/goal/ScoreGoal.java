package edu.ntnu.idatt2001.model.goal;

import edu.ntnu.idatt2001.model.game.Player;

/**
 * A {@code ScoreGoal} is fulfilled when a player has
 * reached a certain amount of score.
 */
public final class ScoreGoal implements Goal {

  private final int points;

  /**
   * Instantiates a new score goal.
   *
   * @param   points the minimum points necessary
   * @throws  IllegalArgumentException if points is less than 1
   */
  public ScoreGoal(int points) {
    if (points < 1) throw new IllegalArgumentException("points must be more than 0");
    this.points = points;
  }

  /**
   * Returns true if the player has the minimum points necessary.
   *
   * @param   player the player
   * @return  true if the player has the minimum points necessary.
   */
  @Override
  public boolean isFulfilled(Player player) {
    return player.getScore() >= points;
  }
}