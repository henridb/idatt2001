package edu.ntnu.idatt2001.model.goal;

import edu.ntnu.idatt2001.model.game.Player;

/**
 * A {@code GoldGoal} is fulfilled when a player has
 * reached a certain amount of gold.
 */
public final class GoldGoal implements Goal {

  private final int gold;

  /**
   * Instantiates a new gold goal.
   *
   * @param   gold the minimum gold
   * @throws  IllegalArgumentException if {@code gold} is less than 1
   */
  public GoldGoal(int gold) {
    if (gold < 1) {
      throw new IllegalArgumentException("gold must be more than 0");
    }
    this.gold = gold;
  }

  /**
   * Returns true if the player has the minimum gold necessary.
   *
   * @param   player the player
   * @return  true if the player has the minimum gold necessary
   */
  @Override
  public boolean isFulfilled(Player player) {
    return player.getGold() >= gold;
  }
}
