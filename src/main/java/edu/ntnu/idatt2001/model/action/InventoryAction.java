package edu.ntnu.idatt2001.model.action;

import edu.ntnu.idatt2001.model.game.Player;
import java.util.Objects;

/**
 * An {@code InventoryAction} adds items to a player's inventory.
 */
public final class InventoryAction implements Action {

  private final String item;

  /**
   * Instantiates a new inventory action.
   *
   * @param   item the item to add
   * @throws  NullPointerException if {@code item} is null
   */
  public InventoryAction(String item) {
    this.item = Objects.requireNonNull(item, "item must not be null");
    if (item.isBlank()) {
      throw new IllegalArgumentException("item must not be blank");
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void execute(Player player) {
    player.addToInventory(item);
  }

  /**
   * Returns the item of this action.
   *
   * @return  the item of this action
   */
  public String getItem() {
    return item;
  }

  @Override
  public String toString() {
    return "inventory:" + item;
  }
}
