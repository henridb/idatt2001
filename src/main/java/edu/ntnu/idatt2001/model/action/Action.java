package edu.ntnu.idatt2001.model.action;

import edu.ntnu.idatt2001.model.game.Player;

/**
 * An {@code Action} executes actions on a given player.
 */
public interface Action {

  /**
   * Executes the action on a player.
   *
   * @param   player the player
   */
  void execute(Player player);
}
