package edu.ntnu.idatt2001.model.action;

import edu.ntnu.idatt2001.model.game.Player;

/**
 * A {@code GoldAction} updates the gold of a player.
 */
public final class GoldAction implements Action {

  private final int gold;

  /**
   * Instantiates a new gold action.
   *
   * @param   gold the gold to add to a player
   * @throws  IllegalArgumentException if {@code gold} is 0
   */
  public GoldAction(int gold) {
    if (gold == 0) {
      throw new IllegalArgumentException("gold must not be 0");
    }
    this.gold = gold;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void execute(Player player) {
    player.addGold(gold);
  }

  @Override
  public String toString() {
    return "gold:" + gold;
  }
}
