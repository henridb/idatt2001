package edu.ntnu.idatt2001.model.action;

import edu.ntnu.idatt2001.model.game.Player;

/**
 * A {@code ScoreAction} updates the score of a player.
 */
public final class ScoreAction implements Action {

  private final int points;

  /**
   * Instantiates a new score action.
   *
   * @param   points points to add
   * @throws  IllegalArgumentException if {@code points} is 0
   */
  public ScoreAction(int points) {
    if (points == 0) throw new IllegalArgumentException("points must not be 0");
    this.points = points;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void execute(Player player) {
    player.addScore(points);
  }

  @Override
  public String toString() {
    return "score:" + points;
  }
}

