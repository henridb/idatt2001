package edu.ntnu.idatt2001.model.action;

import edu.ntnu.idatt2001.model.game.Player;

/**
 * A {@code HealthAction} updates the health of a player.
 */
public final class HealthAction implements Action {

  private final int health;

  /**
   * Instantiates a new health action.
   *
   * @param   health health to add
   * @throws  IllegalArgumentException if {@code health} is 0
   */
  public HealthAction(int health) {
    if (health == 0) {
      throw new IllegalArgumentException("health must not be 0");
    }
    this.health = health;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void execute(Player player) {
    player.addHealth(health);
  }

  @Override
  public String toString() {
    return "health:" + health;
  }
}
