package edu.ntnu.idatt2001.model.game;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A {@code Story} consists of passages that a player can
 * navigate through with the help of links.
 *
 * @see Passage
 * @see Link
 */
public class Story {

  private final String title;
  private final Map<Link, Passage> passages = new HashMap<>();
  private final Passage openingPassage;

  /**
   * Instantiates a new story.
   *
   * @param   title the title of the story
   * @param   passage the first passage in the story
   * @throws  NullPointerException if {@code title} or {@code passage} is null
   * @throws  IllegalArgumentException if {@code title} is blank
   */
  public Story(String title, Passage passage) {
    this.title = Objects.requireNonNull(title, "title must not be null");
    if (title.isBlank()) {
      throw new IllegalArgumentException("title must not be blank");
    }
    this.openingPassage = Objects.requireNonNull(passage, "passage must not be null");
  }

  /**
   * Returns the story title.
   *
   * @return  the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * Returns the opening passage.
   *
   * @return  the opening passage
   */
  public Passage getOpeningPassage() {
    return openingPassage;
  }

  /**
   * Add a passage to the story.
   *
   * @param   passage the passage to add
   * @throws  NullPointerException if {@code passage} is null
   */
  public void addPassage(Passage passage) {
    Objects.requireNonNull(passage, "passage must not be null");
    Link link = new Link(passage.getTitle(), passage.getTitle());
    passages.put(link, passage);
  }

  /**
   * Returns the passage the given link is pointing to.
   *
   * @param   link the link
   * @return  the passage the given link is pointing to
   * @throws  NullPointerException if {@code link} is null
   */
  public Passage getPassage(Link link) {
    Objects.requireNonNull(link, "link must not be null");
    return passages.get(link);
  }

  /**
   * Returns an unmodifiable view of the passages in the story.
   *
   * @return an unmodifiable view of the passages
   */
  public Collection<Passage> getPassages() {
    return Collections.unmodifiableCollection(passages.values());
  }

  /**
   * Removes a link if no passages has a reference to it.
   *
   * @param   link the link
   * @return  true if the passage was removed, otherwise false
   * @throws  NullPointerException if {@code link} is null
   */
  public boolean removePassage(Link link) {
    Objects.requireNonNull(link, "link must not be null");
    if (passages.containsKey(link) && passages.values().stream()
        .flatMap(p -> p.getLinks().stream()).noneMatch(l -> l.equals(link))) {
      return passages.remove(link) != null;
    }
    return false;
  }

  /**
   * Returns a list of links that do not reference any valid passages.
   *
   * @return  a list of broken links
   */
  public List<Link> getBrokenLinks() {
    return passages.values().stream()
        .flatMap(passage -> passage.getLinks().stream())
        .filter(link -> getPassage(link) == null)
        .collect(Collectors.toList());
  }
}
