package edu.ntnu.idatt2001.model.game;

import edu.ntnu.idatt2001.model.goal.Goal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * A {@code Game} consists of a player playing a
 * story with a set of goals to achieve.
 *
 * @see Player
 * @see Story
 * @see Goal
 */
public class Game {

  private final Player player;
  private final Story story;
  private final List<Goal> goals;

  /**
   * Instantiates a new game.
   *
   * @param   player the player that will play the game
   * @param   story the story to follow
   * @param   goals the goals of the game
   * @throws  NullPointerException if {@code player}, {@code story} or {@code goals}
   *          is {@code null}.
   */
  public Game(Player player, Story story, List<Goal> goals) {
    this.player = Objects.requireNonNull(player, "player must not be null");
    this.story = Objects.requireNonNull(story, "story must not be null");
    this.goals = Objects.requireNonNull(goals, "goals must not be null");
  }

  /**
   * Returns the player of this game.
   *
   * @return  the player
   */
  public Player getPlayer() {
    return player;
  }

  /**
   * Returns the story of this game.
   *
   * @return  the story
   */
  public Story getStory() {
    return story;
  }

  /**
   * Add a goal to the story.
   *
   * @param   goal the goal
   * @throws  NullPointerException if {@code goal} is null
   */
  public void addGoal(Goal goal) {
    Objects.requireNonNull(goal, "goal must not be null");
    goals.add(goal);
  }

  /**
   * Returns an unmodifiable view of the goals.
   *
   * @return  an unmodifiable view of the goals
   */
  public List<Goal> getGoals() {
    return Collections.unmodifiableList(goals);
  }

  /**
   * Returns the first passage of the game.
   *
   * @return  the first passage of the game
   */
  public Passage begin() {
    return story.getOpeningPassage();
  }

  /**
   * Returns the passage a link is referencing.
   *
   * @param   link the link for the passage
   * @return  the referenced passage
   * @throws  NullPointerException if {@code link} is null
   */
  public Passage go(Link link) {
    Objects.requireNonNull(link, "link must not be null");
    return story.getPassage(link);
  }
}