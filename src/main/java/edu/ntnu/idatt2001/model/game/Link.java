package edu.ntnu.idatt2001.model.game;

import edu.ntnu.idatt2001.model.action.Action;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * A {@code Link} is used as a connection between two passages.
 * It contains a display text, a reference with the name of the
 * passage it points to, and a list of actions to be applied to
 * a player when the link is activated.
 */
public final class Link {

  private final String text;
  private final String reference;
  private final List<Action> actions = new ArrayList<>();

  /**
   * Instantiates a new link.
   *
   * @param   text the display text of the link
   * @param   reference the name of the passage it points to
   * @throws  NullPointerException if {@code text} is {@code null}
   * @throws  IllegalArgumentException if {@code text} is blank
   */
  public Link(String text, String reference) {
    this.text = Objects.requireNonNull(text, "text must not be null");
    if (text.isBlank()) {
      throw new IllegalArgumentException("text must not be blank");
    }
    this.reference = reference;
  }

  /**
   * Returns the display text.
   *
   * @return  the display text
   */
  public String getText() {
    return text;
  }

  /**
   * Returns the reference.
   *
   * @return  the reference
   */
  public String getReference() {
    return reference;
  }

  /**
   * Add an action to this link.
   *
   * @param   action the action to add
   * @throws  NullPointerException if {@code action} is {@code null}
   */
  public void addAction(Action action) {
    Objects.requireNonNull(action, "action must not be null");
    actions.add(action);
  }

  /**
   * Returns an unmodifiable view of actions for this link.
   *
   * @return  an unmodifiable view of actions for this link
   */
  public List<Action> getActions() {
    return Collections.unmodifiableList(actions);
  }

  /**
   * Returns the hashcode of the reference value.
   *
   * @return  the hash code of this link
   */
  @Override
  public int hashCode() {
    return Objects.hashCode(reference);
  }

  /**
   * Returns true if an object is equal to this link.
   *
   * @param   obj the object to compare to
   * @return  true if an object is equal to this link
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this) return true;
    if (!(obj instanceof Link other)) return false;
    return this.reference.equals(other.reference);
  }

  @Override
  public String toString() {
    return "Link[text=" + text + ", reference=" + reference + "]";
  }
}