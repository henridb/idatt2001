package edu.ntnu.idatt2001.model.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * A {@code Player} is the entity that follows the story
 * and has health, score, gold and an inventory with items.
 */
public final class Player implements Cloneable {

  private final String name;
  private int health;
  private int score;
  private int gold;
  private final List<String> inventory;

  /**
   * Instantiates a new player.
   *
   * @param   name the player name
   * @param   health the initial health
   * @param   score the initial score
   * @param   gold the initial gold
   * @throws  NullPointerException if {@code name} or {@code inventory} is {@code null}
   * @throws  IllegalArgumentException if {@code health}, {@code score} or {@code gold}
   *          is negative.
   */
  public Player(String name, int health, int score, int gold, List<String> inventory) {
    this.name = Objects.requireNonNull(name, "name must not be null");
    if (name.isBlank()) {
      throw new IllegalArgumentException("name must not be blank");
    }
    if (name.length() > 16) {
      throw new IllegalArgumentException("name must not be longer than 16 characters");
    }
    if (health < 1) {
      throw new IllegalArgumentException("health must be more than 0");
    }
    if (score < 0) {
      throw new IllegalArgumentException("score must not be negative");
    }
    if (gold < 0) {
      throw new IllegalArgumentException("gold must not be negative");
    }
    this.health = health;
    this.score = score;
    this.gold = gold;
    this.inventory = Objects.requireNonNull(inventory, "inventory must not be null");
  }

  /**
   * Instantiates a new player from a builder.
   *
   * @param   builder the builder
   * @see     Player#Player(String, int, int, int, List)
   */
  private Player(Builder builder) {
    this(builder.name, builder.health, builder.score, builder.gold, builder.inventory);
  }

  /**
   * Instantiates a new player from another player. Useful for when
   * the game should keep the initial player data so that the user
   * does not have to create a new player from scratch after the game.
   *
   * @param   player the player to copy
   * @see     Player#Player(String, int, int, int, List)
   */
  public Player(Player player) {
    this(player.name, player.health, player.score, player.gold, new ArrayList<>(player.inventory));
  }

  /**
   * Returns the name of the player.
   *
   * @return  the name of the player
   */
  public String getName() {
    return name;
  }

  /**
   * Returns the health of the player.
   *
   * @return  the health of the player
   */
  public int getHealth() {
    return health;
  }

  /**
   * Update the health of the player. If the resulting
   * health is less than 0, the health will be set to 0.
   *
   * @param   health the amount of health to add
   */
  public void addHealth(int health) {
    if (this.health + health < 0) {
      this.health = 0;
      return;
    }
    this.health += health;
  }

  /**
   * Returns the score of the player.
   *
   * @return  the score of the player
   */
  public int getScore() {
    return score;
  }

  /**
   * Update the score that the player has.
   *
   * @param   score the amount of score to add
   */
  public void addScore(int score) {
    this.score += score;
  }

  /**
   * Returns the gold of the player.
   *
   * @return  the gold of the player
   */
  public int getGold() {
    return gold;
  }

  /**
   * Update the gold that the player has.
   *
   * @param   gold the amount of gold to add
   */
  public void addGold(int gold) {
    this.gold += gold;
  }

  /**
   * Add an item to the inventory.
   *
   * @param   item the item to add
   * @throws  NullPointerException if {@code item} is null
   * @throws  IllegalArgumentException if {@code item} is blank
   */
  public void addToInventory(String item) {
    Objects.requireNonNull(item, "item must not be null");
    if (item.isBlank()) {
      throw new IllegalArgumentException("item must not be blank");
    }
    inventory.add(item);
  }

  /**
   * Returns an unmodifiable view of the inventory items.
   *
   * @return  an unmodifiable view of the inventory items
   */
  public List<String> getInventory() {
    return Collections.unmodifiableList(inventory);
  }

  /**
   * A builder class for constructing {@link Player} objects.
   */
  public static final class Builder {

    private String name;
    private int health;
    private int score;
    private int gold;
    private final List<String> inventory = new ArrayList<>();

    /**
     * Set the name of the player.
     *
     * @param   name the player name
     * @return  the builder
     */
    public Builder name(String name) {
      this.name = name;
      return this;
    }

    /**
     * Set the health of the player.
     *
     * @param   health the player health
     * @return  the builder
     */
    public Builder health(int health) {
      this.health = health;
      return this;
    }

    /**
     * Set the score of the player.
     *
     * @param   score the player score
     * @return  the builder
     */
    public Builder score(int score) {
      this.score = score;
      return this;
    }

    /**
     * Set the gold of the player.
     *
     * @param   gold the player gold
     * @return  the builder
     */
    public Builder gold(int gold) {
      this.gold = gold;
      return this;
    }

    /**
     * Build a new {@link Player} from the builder.
     *
     * @return  the player
     */
    public Player build() {
      return new Player(this);
    }
  }
}