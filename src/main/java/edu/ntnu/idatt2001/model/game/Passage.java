package edu.ntnu.idatt2001.model.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * A {@code Passage} represents a point in the story where
 * the player must make a choice between links for further
 * progress. Each passage must have a title and content,
 * however images and time limits are optional.
 *
 * @see Link
 */
public final class Passage {

  private final String title;
  private final String content;
  private final String imagePath;
  private final int time;
  private final List<Link> links;

  public Passage(String title, String content) {
    this(title, content, null, 0, new ArrayList<>());
  }

  /**
   * Instantiates a new passage.
   *
   * @param   title the passage title
   * @param   content the passage content
   * @param   imagePath a path to an image
   * @param   time the time limit
   * @throws  NullPointerException if {@code title} or {@code content} is {@code null}
   * @throws  IllegalArgumentException if {@code title} or {@code content} is
   *          blank or {@code time} is negative.
   */
  public Passage(String title, String content, String imagePath, int time, List<Link> links) {
    this.title = Objects.requireNonNull(title, "title must not be null");
    if (title.isBlank()) {
      throw new IllegalArgumentException("title must not be blank");
    }
    this.content = Objects.requireNonNull(content, "content must not be null");
    if (content.isBlank()) {
      throw new IllegalArgumentException("content must not be blank");
    }
    if (imagePath != null && imagePath.isBlank()) {
      throw new IllegalArgumentException("image path must not be blank");
    }
    this.imagePath = imagePath;
    if (time < 0) {
      throw new IllegalArgumentException("time must not be negative");
    }
    this.time = time;
    this.links = links;
  }

  /**
   * Instantiates a passage from the builder.
   *
   * @param   builder the builder
   * @see     Passage#Passage(String, String, String, int, List)
   */
  private Passage(Builder builder) {
    this(builder.title, builder.content, builder.imagePath, builder.time, builder.links);
  }

  /**
   * Returns the title of the passage.
   *
   * @return  the title of the passage
   */
  public String getTitle() {
    return title;
  }

  /**
   * Returns the content of the passage.
   *
   * @return  the content of the passage
   */
  public String getContent() {
    return content;
  }

  /**
   * Returns the path to the image associated with this passage.
   *
   * @return  the path to the image associated with this passage
   */
  public String getImagePath() {
    return imagePath;
  }

  /**
   * Returns true if the passage has an image.
   *
   * @return  true if the passage has an image, otherwise false
   */
  public boolean hasImage() {
    return imagePath != null;
  }

  /**
   * Returns the time limit in seconds.
   *
   * @return  the time limit in seconds
   */
  public int getTime() {
    return time;
  }

  /**
   * Returns true if the passage has a time limit.
   *
   * @return  true if the passage has a time limit
   */
  public boolean isTimed() {
    return time > 0;
  }

  /**
   * Add a link to this passage.
   *
   * @param   link the link to add
   * @return  true if the link was added, otherwise false
   * @throws  NullPointerException if {@code link} is {@code null}
   */
  public boolean addLink(Link link) {
    Objects.requireNonNull(link, "link must not be null");
    if (links.contains(link)) {
      return false;
    }
    links.add(link);
    return true;
  }

  /**
   * Returns an unmodifiable view of the links in this passage.
   *
   * @return  an unmodifiable view of the links in this passage
   */
  public List<Link> getLinks() {
    return Collections.unmodifiableList(links);
  }

  /**
   * Returns true if the passage contains any links.
   *
   * @return  true if the passage contains any links
   */
  public boolean hasLinks() {
    return !links.isEmpty();
  }

  /**
   * Returns the hash code of the title.
   *
   * @return  the hash code of the passage
   */
  @Override
  public int hashCode() {
    return Objects.hashCode(title);
  }

  /**
   * Returns true if an object is equal to this passage.
   *
   * @param   obj the object to compare to
   * @return  true if an object is equal to this passage
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this) return true;
    if (!(obj instanceof Passage other)) return false;
    return this.title.equals(other.title);
  }

  @Override
  public String toString() {
    return "Passage[title=" + title + ", content=" + content + ", imagePath=" + imagePath
        + ", time=" + time + ", links=" + links + "]";
  }

  /**
   * A builder class for constructing {@link Passage} objects.
   */
  public static final class Builder {

    private String title;
    private String content;
    private String imagePath;
    private int time;
    private final List<Link> links = new ArrayList<>();

    /**
     * Set the title of the passage.
     *
     * @param   title the title
     * @return  the builder
     */
    public Builder title(String title) {
      this.title = title;
      return this;
    }

    /**
     * Set the content of the passage.
     *
     * @param   content the content
     * @return  the builder
     */
    public Builder content(String content) {
      this.content = content;
      return this;
    }

    /**
     * Set the image path of the passage.
     *
     * @param   imagePath the image path
     * @return  the builder
     */
    public Builder imagePath(String imagePath) {
      this.imagePath = imagePath;
      return this;
    }

    /**
     * Set the time limit of the passage.
     *
     * @param   time the time
     * @return  the builder
     */
    public Builder time(int time) {
      this.time = time;
      return this;
    }

    /**
     * Add a link to the passage.
     *
     * @param   link the link
     * @return  the builder
     */
    public Builder addLink(Link link) {
      this.links.add(link);
      return this;
    }

    /**
     * Build a new {@link Passage} from the builder.
     *
     * @return the passage
     */
    public Passage build() {
      return new Passage(this);
    }
  }
}
