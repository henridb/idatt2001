package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.page.HomePage;
import edu.ntnu.idatt2001.page.Page;
import java.util.Objects;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 * The main application class.
 */
public class Paths extends Application {

  public static final int HEIGHT = 900;
  public static final int WIDTH = 650;

  private static Stage stage;

  @Override
  public void start(Stage stage) {
    stage.show();
    stage.setTitle("Paths");
    stage.setMinWidth(HEIGHT);
    stage.setMinHeight(WIDTH);
    stage.getIcons().add(new Image(Objects.requireNonNull(
        Paths.class.getResourceAsStream("/images/logo.png"))));
    Paths.stage = stage;
    setPage(new HomePage());
  }

  /**
   * Returns the background image.
   *
   * @return  the background image
   */
  public static Background getBackground() {
    Image image = new Image(Objects.requireNonNull(
        Paths.class.getResourceAsStream("/images/background.png")));
    BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT,
        BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
        new BackgroundSize(100, 100, true, true, true, true));
    return new Background(backgroundImage);
  }

  /**
   * Set the current page.
   *
   * @param   page the page
   * @throws  NullPointerException if {@code page} is null
   * @see     Page
   */
  public static void setPage(Page page) {
    Objects.requireNonNull(page, "page must not be null");
    page.load(stage);
  }

  /**
   * Launch the JavaFX application.
   *
   * @param   args the program args
   */
  public static void main(String[] args) {
    launch();
  }
}