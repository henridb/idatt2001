package edu.ntnu.idatt2001.model.action;

import edu.ntnu.idatt2001.model.action.InventoryAction;
import edu.ntnu.idatt2001.model.game.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Inventory Action")
public class InventoryActionTest {

  private final Player player = new Player("Player", 100, 0, 0, new ArrayList<>());

  @Nested
  class ConstructorTest {

    @Test
    @DisplayName("Item not empty or null")
    public void itemIsNotEmptyOrNullTest() {
      assertDoesNotThrow(() -> new InventoryAction("Test item"));
    }

    @Test
    @DisplayName("Item is empty or null")
    public void itemIsEmptyOrNullTest() {
      assertThrows(IllegalArgumentException.class, () -> new InventoryAction(""));
    }
  }

  @Nested
  class InventoryExecute {

    @Test
    @DisplayName("Item added to inventory")
    public void itemIsAddedTest() {
      new InventoryAction("Test").execute(player);
      assertTrue(player.getInventory().contains("Test"));
    }

    @Test
    @DisplayName("Empty item not added")
    public void itemIsNotAddedTest() {
      assertThrows(IllegalArgumentException.class, () -> new InventoryAction("").execute(player));
    }

    @Test
    @DisplayName("Get item")
    public void getItemTest() {
      assertEquals("Item", new InventoryAction("Item").getItem());
    }
  }
}
