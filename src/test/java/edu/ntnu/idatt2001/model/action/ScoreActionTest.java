package edu.ntnu.idatt2001.model.action;

import edu.ntnu.idatt2001.model.action.ScoreAction;
import edu.ntnu.idatt2001.model.game.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Score Action Tests")
public class ScoreActionTest {

  private final Player player = new Player("Player", 100, 0, 0, new ArrayList<>());

  @Nested
  class ConstructorTest {

    @Test
    @DisplayName("Positive score test")
    public void scoreIsPositiveTest() {
      assertDoesNotThrow(() -> new ScoreAction(100));
    }

    @Test
    @DisplayName("Zero score test")
    public void scoreIsNegativeTest() {
      assertThrows(IllegalArgumentException.class, () -> new ScoreAction(0));
    }
  }

  @Nested
  class ScoreExecute {

    @Test
    @DisplayName("Score is added test")
    public void scoreIsAddedTest() {
      new ScoreAction(100).execute(player);
      assertEquals(100, player.getScore());
    }

    @Test
    @DisplayName("Zero score is not added test")
    public void scoreIsNotAddedTest() {
      assertThrows(IllegalArgumentException.class, () -> new ScoreAction(0).execute(player));
    }
  }
}
