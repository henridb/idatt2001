package edu.ntnu.idatt2001.model.action;

import edu.ntnu.idatt2001.model.action.GoldAction;
import edu.ntnu.idatt2001.model.game.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Gold Action")
public class GoldActionTest {

  private final Player player = new Player("Player", 100, 0, 0, new ArrayList<>());

  @Nested
  class ConstructorTest {

    @Test
    @DisplayName("Positive gold")
    public void testPositiveGold() {
      assertDoesNotThrow(() -> new GoldAction(10));
    }

    @Test
    @DisplayName("Zero gold")
    public void testNegativeGold() {
      assertThrows(IllegalArgumentException.class, () -> new GoldAction(0));
    }
  }

  @Nested
  class GoldExecuteTest {

    @Test
    @DisplayName("Can add gold")
    public void goldIsAddedTest() {
      new GoldAction(100).execute(player);
      assertEquals(100, player.getGold());
    }

    @Test
    @DisplayName("Can not add zero gold")
    public void goldIsNotAddedTest() {
      assertThrows(IllegalArgumentException.class, () -> new GoldAction(0).execute(player));
    }
  }
}
