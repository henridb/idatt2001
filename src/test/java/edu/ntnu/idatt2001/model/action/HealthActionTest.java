package edu.ntnu.idatt2001.model.action;

import edu.ntnu.idatt2001.model.action.HealthAction;
import edu.ntnu.idatt2001.model.game.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Health Action")
public class HealthActionTest {

  private final Player player = new Player("Player", 100, 0, 0, Collections.emptyList());

  @Nested
  class ConstructorTest {

    @Test
    @DisplayName("Positive health test")
    public void testPositiveHealth() {
      assertDoesNotThrow(() -> new HealthAction(100));
    }

    @Test
    @DisplayName("Zero health test")
    public void testNegativeHealth() {
      assertThrows(IllegalArgumentException.class, () -> new HealthAction(0));
    }
  }
  @Nested
  class HealthExecuteTest {

    @Test
    @DisplayName("Can add positive health test")
    public void healthIsAddedTest() {
      new HealthAction(100).execute(player);
      assertEquals(200, player.getHealth());
    }

    @Test
    @DisplayName("Can not add zero health test")
    public void healthIsNotAddedTest() {
      assertThrows(IllegalArgumentException.class, () -> new HealthAction(0).execute(player));
    }
  }
}
