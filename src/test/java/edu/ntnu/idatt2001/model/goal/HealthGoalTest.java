package edu.ntnu.idatt2001.model.goal;

import edu.ntnu.idatt2001.model.goal.Goal;
import edu.ntnu.idatt2001.model.goal.HealthGoal;
import edu.ntnu.idatt2001.model.game.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Health Goal")
public class HealthGoalTest {

  private final Player player = new Player("Player", 100, 50, 10, Collections.emptyList());

  @Nested
  class Constructor {

    @Test
    @DisplayName("Positive health")
    public void testPositive() {
      assertDoesNotThrow(() -> new HealthGoal(10));
    }

    @Test
    @DisplayName("Negative health")
    public void testNegative() {
      assertThrows(IllegalArgumentException.class, () -> new HealthGoal(-10));
    }

    @Test
    @DisplayName("Zero health")
    public void testZero() {
      assertThrows(IllegalArgumentException.class, () -> new HealthGoal(0));
    }
  }

  @Test
  @DisplayName("Fulfilled")
  public void testFulfilled() {
    Goal goal = new HealthGoal(10);
    assertTrue(goal.isFulfilled(player));
  }

  @Test
  @DisplayName("Not fulfilled")
  public void testNotFulfilled() {
    Goal goal = new HealthGoal(150);
    assertFalse(goal.isFulfilled(player));
  }
}
