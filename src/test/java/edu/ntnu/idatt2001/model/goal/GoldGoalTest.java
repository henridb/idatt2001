package edu.ntnu.idatt2001.model.goal;

import edu.ntnu.idatt2001.model.goal.Goal;
import edu.ntnu.idatt2001.model.goal.GoldGoal;
import edu.ntnu.idatt2001.model.game.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Gold Goal")
public class GoldGoalTest {

  private final Player player = new Player("Player", 100, 50, 10, Collections.emptyList());

  @Nested
  class Constructor {

    @Test
    @DisplayName("Positive gold")
    public void testPositive() {
      assertDoesNotThrow(() -> new GoldGoal(10));
    }

    @Test
    @DisplayName("Negative gold")
    public void testNegative() {
      assertThrows(IllegalArgumentException.class, () -> new GoldGoal(-10));
    }

    @Test
    @DisplayName("Zero gold")
    public void testZero() {
      assertThrows(IllegalArgumentException.class, () -> new GoldGoal(0));
    }
  }

  @Test
  @DisplayName("Fulfilled")
  public void testFulfilled() {
    Goal goal = new GoldGoal(10);
    assertTrue(goal.isFulfilled(player));
  }

  @Test
  @DisplayName("Not fulfilled")
  public void testNotFulfilled() {
    Goal goal = new GoldGoal(11);
    assertFalse(goal.isFulfilled(player));
  }
}