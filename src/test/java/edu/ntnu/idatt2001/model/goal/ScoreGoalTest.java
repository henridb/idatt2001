package edu.ntnu.idatt2001.model.goal;

import edu.ntnu.idatt2001.model.goal.Goal;
import edu.ntnu.idatt2001.model.goal.ScoreGoal;
import edu.ntnu.idatt2001.model.game.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Score Goal")
public class ScoreGoalTest {

  private final Player player = new Player("Player", 100, 50, 10, Collections.emptyList());

  @Nested
  class Constructor {

    @Test
    @DisplayName("Positive score")
    public void testPositive() {
      assertDoesNotThrow(() -> new ScoreGoal(10));
    }

    @Test
    @DisplayName("Negative score")
    public void testNegative() {
      assertThrows(IllegalArgumentException.class, () -> new ScoreGoal(-10));
    }

    @Test
    @DisplayName("Zero score")
    public void testZero() {
      assertThrows(IllegalArgumentException.class, () -> new ScoreGoal(0));
    }
  }

  @Test
  @DisplayName("Fulfilled")
  public void testFulfilled() {
    Goal goal = new ScoreGoal(10);
    assertTrue(goal.isFulfilled(player));
  }

  @Test
  @DisplayName("Not fulfilled")
  public void testNotFulfilled() {
    Goal goal = new ScoreGoal(150);
    assertFalse(goal.isFulfilled(player));
  }
}
