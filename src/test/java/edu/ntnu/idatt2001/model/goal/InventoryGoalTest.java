package edu.ntnu.idatt2001.model.goal;

import edu.ntnu.idatt2001.model.goal.Goal;
import edu.ntnu.idatt2001.model.goal.InventoryGoal;
import edu.ntnu.idatt2001.model.game.Player;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Inventory Goal")
public class InventoryGoalTest {

  private static final Player player = new Player("Player", 100, 50, 10, new ArrayList<>());
  private final List<String> items = Collections.singletonList("Item 1");

  @BeforeAll
  public static void setupPlayer() {
    player.addToInventory("Item 1");
  }

  @Nested
  class Constructor {

    @Test
    @DisplayName("Empty item list")
    public void testEmpty() {
      assertDoesNotThrow(() -> new InventoryGoal(items));
    }
  }

  @Test
  @DisplayName("Fulfilled")
  public void testFulfilled() {
    Goal goal = new InventoryGoal(items);
    assertTrue(goal.isFulfilled(player));
  }

  @Test
  @DisplayName("Not fulfilled")
  public void testNotFulfilled() {
    Goal goal = new InventoryGoal(Collections.singletonList("Item 2"));
    assertFalse(goal.isFulfilled(player));
  }
}
