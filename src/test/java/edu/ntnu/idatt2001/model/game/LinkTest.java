package edu.ntnu.idatt2001.model.game;

import edu.ntnu.idatt2001.model.action.GoldAction;
import edu.ntnu.idatt2001.model.game.Link;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Link Test")
public class LinkTest {

  private final String text = "Text";
  private final String reference = "Reference";

  @Nested
  class Constructor {

    @Test
    @DisplayName("Empty text")
    public void testEmpty() {
      assertThrows(IllegalArgumentException.class, () -> new Link("", ""));
    }

    @Test
    @DisplayName("Null text")
    public void testNull() {
      assertThrows(NullPointerException.class, () -> new Link(null, null));
    }
  }

  @Test
  @DisplayName("Text")
  public void getTextTest() {
    Link link = new Link(text, reference);
    assertEquals(text, link.getText());
  }

  @Test
  @DisplayName("Reference")
  public void getReferenceTest() {
    Link link = new Link(text, reference);
    assertEquals(reference, link.getReference());
  }

  @Test
  @DisplayName("Add action")
  public void addActionTest() {
    Link link = new Link(text, reference);
    link.addAction(new GoldAction(10));
    assertEquals(1, link.getActions().size());
  }

  @Test
  @DisplayName("Equals")
  public void actionEqualsTest() {
    Link link1 = new Link(text, reference);
    Link link2 = new Link(text, reference);
    assertEquals(link1, link2);
  }

  @Test
  @DisplayName("Not equals")
  public void actionNotEqualsTest() {
    Link link1 = new Link(text, reference);
    Link link2 = new Link(text, reference + " test");
    assertNotEquals(link1, link2);
  }
}