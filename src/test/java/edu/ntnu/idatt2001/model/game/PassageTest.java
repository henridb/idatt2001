package edu.ntnu.idatt2001.model.game;

import edu.ntnu.idatt2001.model.game.Link;
import edu.ntnu.idatt2001.model.game.Passage;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Passage Test")
public class PassageTest {

  private final String title = "Title";
  private final String content = "Content";

  @Nested
  class Constructor {

    @Test
    @DisplayName("Empty title")
    public void testEmptyTitle() {
      assertThrows(IllegalArgumentException.class, () -> new Passage("", content));
    }

    @Test
    @DisplayName("Empty content")
    public void testEmptyContent() {
      assertThrows(IllegalArgumentException.class, () -> new Passage(title, ""));
    }

    @Test
    @DisplayName("Empty image path")
    public void testImagePath() {
      assertThrows(IllegalArgumentException.class, () -> new Passage(title, content, "", 10, new ArrayList<>()));
    }

    @Test
    @DisplayName("Negative time")
    public void negativeTime() {
      assertThrows(IllegalArgumentException.class, () -> new Passage(title, content, null, -10, new ArrayList<>()));
    }

    @Test
    @DisplayName("Null title")
    public void testNullTitle() {
      assertThrows(NullPointerException.class, () -> new Passage(null, content));
    }

    @Test
    @DisplayName("Null content")
    public void testNullContent() {
      assertThrows(NullPointerException.class, () -> new Passage(title, null));
    }
  }

  @Test
  @DisplayName("Title")
  public void getTitleTest() {
    Passage passage = new Passage(title, content);
    assertEquals(title, passage.getTitle());
  }

  @Test
  @DisplayName("Content")
  public void getContentTest() {
    Passage passage = new Passage(title, content);
    assertEquals(content, passage.getContent());
  }

  @Test
  @DisplayName("Add link")
  public void addLinkTest() {
    Link link = new Link("Text", "Reference");
    Passage passage = new Passage(title, content);
    passage.addLink(link);
    assertEquals(1, passage.getLinks().size());
  }

  @Test
  @DisplayName("Has links")
  public void hasLinksTest() {
    Link link = new Link("Text", "Reference");
    Passage passage = new Passage(title, content);
    passage.addLink(link);
    assertTrue(passage.hasLinks());
  }

  @Test
  @DisplayName("Equals")
  public void actionEqualsTest() {
    Passage passage1 = new Passage(title, content);
    Passage passage2 = new Passage(title, content);
    assertEquals(passage1, passage2);
  }

  @Test
  @DisplayName("Not equals")
  public void actionNotEqualsTest() {
    Passage passage1 = new Passage(title, content);
    Passage passage2 = new Passage(title + "test", content);
    assertNotEquals(passage1, passage2);
  }
}