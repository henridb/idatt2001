package edu.ntnu.idatt2001.model.game;

import edu.ntnu.idatt2001.model.game.Link;
import edu.ntnu.idatt2001.model.game.Passage;
import edu.ntnu.idatt2001.model.game.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Story Test")
public class StoryTest {

  private final String title = "Title";
  private final Passage openingPassage = new Passage("Title", "Content");

  @Nested
  class Constructor {

    @Test
    @DisplayName("Empty title")
    public void emptyTitleTest() {
      assertThrows(IllegalArgumentException.class, () -> new Story("", openingPassage));
    }

    @Test
    @DisplayName("Null title")
    public void nullTitleTest() {
      assertThrows(NullPointerException.class, () -> new Story(null, openingPassage));
    }

    @Test
    @DisplayName("No opening passage")
    public void noOpeningPassage() {
      assertThrows(NullPointerException.class, () -> new Story(title, null));
    }
  }

  @Test
  @DisplayName("Title")
  public void getTitleTest() {
    Story story = new Story(title, openingPassage);
    assertEquals(title, story.getTitle());
  }

  @Test
  @DisplayName("Opening passage")
  public void getOpeningPassageTest() {
    Story story = new Story(title, openingPassage);
    assertEquals(openingPassage, story.getOpeningPassage());
  }

  @Test
  @DisplayName("Add passage")
  public void addPassageTest() {
    Story story = new Story(title, openingPassage);
    Passage passage = new Passage("Title", "Content");
    Link link = new Link("Title", "Title");
    story.addPassage(passage);
    assertEquals(passage, story.getPassage(link));
  }

  @Test
  @DisplayName("Remove passage")
  public void removePassageTest() {
    Story story = new Story(title, openingPassage);
    Passage passage = new Passage("Passage", "Content");
    Link link = new Link("Passage Link", "Passage");
    passage.addLink(link);
    story.addPassage(passage);
    assertFalse(story.removePassage(link));
  }
}
