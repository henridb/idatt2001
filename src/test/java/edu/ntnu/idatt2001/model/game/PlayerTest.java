package edu.ntnu.idatt2001.model.game;

import edu.ntnu.idatt2001.model.game.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Player Test")
public class PlayerTest {

  private final Player player = new Player("Player", 100, 10, 5, new ArrayList<>());

  @Nested
  class Constructor {

    @Test
    @DisplayName("Empty name")
    public void emptyNameTest() {
      assertThrows(IllegalArgumentException.class, () -> new Player("", 0, 0, 0, Collections.emptyList()));
    }

    @Test
    @DisplayName("Negative health")
    public void negativeHealthTest() {
      assertThrows(IllegalArgumentException.class, () -> new Player("Player", -1, 0, 0, Collections.emptyList()));
    }

    @Test
    @DisplayName("Negative score")
    public void negativeScoreTest() {
      assertThrows(IllegalArgumentException.class, () -> new Player("Player", 0, -1, 0, Collections.emptyList()));
    }

    @Test
    @DisplayName("Negative gold")
    public void negativeGoldTest() {
      assertThrows(IllegalArgumentException.class, () -> new Player("Player", 0, 0, -1, Collections.emptyList()));
    }
  }

  @Test
  @DisplayName("Get name")
  public void getNameTest() {
    assertEquals("Player", player.getName());
  }

  @Test
  @DisplayName("Add health")
  public void addHealthTest() {
    player.addHealth(20);
    assertEquals(120, player.getHealth());
  }

  @Test
  @DisplayName("Add negative health")
  public void addNegativeHealthTest() {
    player.addHealth(-1000);
    assertEquals(0, player.getHealth());
  }

  @Test
  @DisplayName("Add score")
  public void addScoreTest() {
    player.addScore(20);
    assertEquals(30, player.getScore());
  }

  @Test
  @DisplayName("Add gold")
  public void addGoldTest() {
    player.addGold(20);
    assertEquals(25, player.getGold());
  }

  @Test
  @DisplayName("Add item")
  public void addItemTest() {
    player.addToInventory("Item 1");
    assertTrue(player.getInventory().contains("Item 1"));
  }

  @Test
  @DisplayName("Add blank item")
  public void addBlankItemTest() {
    assertThrows(IllegalArgumentException.class, () -> player.addToInventory(""));
  }

  @Test
  @DisplayName("Edit unmodifiable inventory")
  public void addItemTest2() {
    assertThrows(UnsupportedOperationException.class, () -> player.getInventory().add("Item 2"));
  }

  @Test
  @DisplayName("Player builder")
  public void playerBuilderTest() {
    Player b =  new Player.Builder()
        .name("Name")
        .health(100)
        .score(30)
        .gold(20)
        .build();
    assertEquals("Name", b.getName());
    assertEquals(100, b.getHealth());
    assertEquals(30, b.getScore());
    assertEquals(20, b.getGold());
  }
}
