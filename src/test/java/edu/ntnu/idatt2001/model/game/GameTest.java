package edu.ntnu.idatt2001.model.game;

import edu.ntnu.idatt2001.model.game.*;
import edu.ntnu.idatt2001.model.goal.GoldGoal;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Game Test")
public class GameTest {

  private final Player player = new Player("Player", 100, 10, 5, new ArrayList<>());

  private final String title = "Title";
  private final Passage openingPassage = new Passage("Title", "Content");
  private final Story story = new Story(title, openingPassage);
  private final Game game = new Game(player, story, new ArrayList<>());

  @Nested
  class Constructor {

    @Test
    @DisplayName("Null player")
    public void nullPlayerTest() {
      assertThrows(NullPointerException.class, () -> new Game(null, story, new ArrayList<>()));
    }

    @Test
    @DisplayName("Null story")
    public void nullStoryTest() {
      assertThrows(NullPointerException.class, () -> new Game(player, null, new ArrayList<>()));
    }

    @Test
    @DisplayName("Null goals")
    public void nullGoalsTest() {
      assertThrows(NullPointerException.class, () -> new Game(player, story, null));
    }
  }

  @Test
  @DisplayName("Get player")
  public void getPlayerTest() {
    assertEquals(player, game.getPlayer());
  }

  @Test
  @DisplayName("Get story")
  public void getStoryTest() {
    assertEquals(story, game.getStory());
  }

  @Test
  @DisplayName("Add null goal")
  public void addNullGoal() {
    assertThrows(NullPointerException.class, () -> game.addGoal(null));
  }

  @Test
  @DisplayName("Add goal")
  public void addGoal() {
    game.addGoal(new GoldGoal(10));
    assertEquals(1, game.getGoals().size());
  }

  @Test
  @DisplayName("Get opening passage")
  public void getOpeningPassageTest() {
    assertEquals(openingPassage, game.begin());
  }

  @Test
  @DisplayName("Go to passage")
  public void getToPassageTest() {
    game.getStory().addPassage(openingPassage);
    assertEquals(openingPassage, game.go(new Link("Test", "Title")));
  }
}
