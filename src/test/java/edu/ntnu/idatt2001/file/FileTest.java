package edu.ntnu.idatt2001.file;

import edu.ntnu.idatt2001.model.game.Link;
import edu.ntnu.idatt2001.model.game.Passage;
import edu.ntnu.idatt2001.model.game.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("File Test")
public class FileTest {

  private final File testStory = new File("teststory.paths");;

  @Test
  @DisplayName("Write story")
  public void writeStoryTest() throws IOException {
    Story story = FileManager.readStory(testStory);
    FileManager.toFile(story, new File("newStory.paths"));
  }

  @Test
  @DisplayName("Broken links")
  public void BrokenLinksTest() {
    Passage passage = new Passage("Passage", "Content");
    passage.addLink(new Link("Text", "Reference"));
    Story story = new Story("Title", passage);
    story.addPassage(passage);
    assertThrows(IllegalArgumentException.class, () -> FileManager.checkBrokenLinks(story));
  }

  @Test
  @DisplayName("Read story")
  public void readStoryTest() {
    assertDoesNotThrow(() -> FileManager.readStory(testStory));
  }

  @Test
  @DisplayName("Read null file")
  public void readNullStoryTest() {
    assertThrows(NullPointerException.class, () -> FileManager.readStory(null));
  }

  @Test
  @DisplayName("Read invalid file")
  public void readInvalidStoryTest() {
    assertThrows(IllegalArgumentException.class, () -> FileManager.readStory(new File("test.txt")));
  }
}
