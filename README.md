# IDATT2001 - Paths

Paths is a project for the 2023 spring semester in the course Programmering 2.

## Installation

Clone the project to your IDE and run the application with ```javafx:run```. This
option requires the Java Development Kit (JDK) with a minimum version of 17.