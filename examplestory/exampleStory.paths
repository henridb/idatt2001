The Kidnapping

title:Beginning
content:You wake up on random morning to a knock at the door. The person outside sounds desperate and panicked. What do you do?
time:120
[Answer the door](Answer the door){score:10}
[Ignore the person](Ignore the person){}

title:Answer the door
content:The person introduces themselves as a police officer. Apparently a high ranking member of the royal family has been kidnapped. Your help is requested right away. Do you accept?
time:120
[Accept](Accept){inventory:Police badge}
[Decline](Decline){score:-10}

title:Ignore the person
content:The person eventually goes away. You get dressed and stroll into town. You get to a crossroad and have to make a decision where to go.
time:120
[Go to the Black market](Go to the Black market){}
[Go to police station](Go to police station){score:10}

title:Accept
content:The person seems relieved that you have accepted. They inform you it is the princess who has been kidnapped. You have a decision to make where to look first.
time:120
[Where the princess was last seen](Where the princess was last seen){}
[The police's main suspect](The police's main suspect){}

title:Decline
content: The police officer looks at you disappointment, as you turn to go about your day, you hear a commotion behind you. The officer accuses you of obstructing justice and arrests you on the spot. GAME OVER %name%

title:Go to the Black market
content:On your way to the Black market you walk through a dark alleyway. A person steps in front of you and demands your wallet. What do you do?
time:30
[Run away](Run away){health:-20}
[Give up your wallet](Give up your wallet){score:-10}

title:Go to police station
content:A police officer rushes over to you and claims they were at your door this morning. He informs you a high ranking member of the royal family has been kidnapped. He demands your help.
time:120
[Accept](Accept){}
[Decline](Decline){}

title:Run away
content:The person catches up to you quickly and robs you anyway. He tells you to turn around and go the other direction.
time:60
[Go to the police station](Go to police station){}
[Ignore the man](Ignore the man){health:-120}

title:Ignore the man
content:Disregarding the man's demand, you continue walking, assuming it's just an empty threat. However, he was not bluffing. In a split second, a gunshot echoes through the alley, and pain sears through your body. Everything fades to darkness as your life comes to an abrupt end.

title:Where the princess was last seen
content:You gather information about the last known location of the princess. According to eyewitnesses, she was seen near the old abandoned castle on the outskirts of town. It's rumored to be haunted, but you know that sometimes rumors hold a grain of truth. What do you do?
time:60
[Investigate the old abandoned castle](Investigate the castle){inventory: Lantern}
[Question the eyewitnesses](Question eyewitnesses){score:10}

title:The police's main suspect
content:As you dig deeper into the investigation, you discover a lead that points towards a notorious criminal known as "The Shadow." Rumors swirl about this enigmatic figure, and many believe they have ties to the royal family's kidnapping. Gathering evidence and unraveling the mysteries surrounding "The Shadow" could bring you one step closer to finding the missing princess. What's your next move?
time:60
[Confront "The Shadow"](Confront "The Shadow"){}
[Check out where the princess was last seen](Where the princess was last seen){}

title:Confront "The Shadow"
content: You seek out "the Shadow's" hideout and discover that he has a perfect alibi for the night of the kidnapping. He tells you not to trust the police officer as he has known ties in the criminal underworld.
time:100
[Check out where the princess was last seen](Where the princess was last seen){}

title:Give up your wallet
content:You reluctantly hand over your wallet, realizing that fighting against the odds in this situation might not be the best course of action. The thief swiftly takes your belongings but let's you advance to the Black market.
time:100
[Continue to the Black market](Continue to the Black market){score:10}

title:Continue to the Black market
content: Choose an item on the Black market before heading to the police station.
time:100
[Revolver](Go to police station){inventory:Revolver}
[Sword](Go to police station){inventory:Sword}
[Shield](Go to police station){inventory:Shield}
[Body armor](Go to police station){inventory:Body Armor}

title:Investigate the castle
content:The police officer seems hesitant to explore the castle and explains that it's rumored to be haunted. He suggests searching in the upper towers first. Where in the castle do you search first?
time:60
[Explore the dungeon](Explore dungeon){}
[Explore the upper tower](Explore tower){}

title:Question eyewitnesses
content:You approach the eyewitnesses who claimed to have seen the princess last. They provide conflicting accounts, but one name keeps resurfacing, Frederick. A known troublemaker with connections to the criminal underworld. The police officer seems unsure about this information and tells you Fredrick is harmless. How do you proceed?
time:30
[Trust the police officer](Trust police officer){}
[Investigate abandoned castle](Investigate the castle){}

title:Trust police officer
content:The police officer convinces you to explore the dungeon of the abandoned castle.
time:60
[Explore the dungeon of the abandoned castle](Explore dungeon){score:-100,health:-100}

title:Explore dungeon
content:Together with the police officer you head down in the dungeon. The dungeon is dark and you can barely make a figure in the corner. You find the princess tied up and afraid. The police officer turns his gun on you and everything turns to black.

title:Explore tower
content: The tower shows signs of a struggle. The police officer seems eager to leave. You find a police badge in the corner with the police officers name on it. What do you want to do?
time:60
[Question the police officer](Question the police officer){}
[Explore the dungeon](Explore dungeon){}

title:Question the police officer
content: You ask the police officer why his police badge is in the tower. He answers that "The Shadow" must be trying to frame him for the kidnapping. He tells you that this is a waste of time and asks to head down to dungeon.
time:30
[Trust the police officer](Trust police officer){}
[Arrest the police officer](Arrest the police officer){score:100}

title:Arrest the police officer
content:You successfully apprehended the right suspect. The police officer kidnapped the princess and tried to frame "The Shadow" using you. Luckily you were not fooled. Congratulations %name%!
